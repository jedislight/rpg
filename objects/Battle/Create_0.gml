/// @description Insert description here
// You can write your code in this editor

input_lock_set("Battle", true)

initiative = ds_list_create();
enemies = ds_list_create(); // which enemies to copy
enemies_copies = ds_list_create();

turn = 0

combatants_added = false
battle_over = false;
initiative_index = 0;
turn_taken = false;
turn_started = false;
manual_target = ""
battle_no_rewards = false;

// drawing offsets
enemy_base_xx = 64;
enemy_base_yy = 64;
enemy_tile_width = 80;
enemy_tile_height = 128;
pc_tile_width = 80;
pc_tile_height = 96;
pc_base_xx = 512-32-pc_tile_width;
pc_base_yy = enemy_base_yy;

music_push_track(music_Random_Battle);