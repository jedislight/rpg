/// @description Insert description here
// You can write your code in this editor

if ((battle_over or not combatants_added) and not battle_is_visual_playing())
{
	exit;	
}

turn_left_initiative = ds_list_create();
ds_list_copy(turn_left_initiative, initiative)

for(var i = 0; i < initiative_index; ++i)
{
	ds_list_delete(turn_left_initiative, 0)
}

initiative_string = ds_list_join(turn_left_initiative, " ");


var bg_color1 = c_black;
var bg_color2 = c_dkgray;
var bg_color3 = c_black;
var bg_color4 = c_gray;

draw_rectangle_color_analagous_outline(32, 32, 512-32, 512-32, bg_color1, bg_color2, bg_color3, bg_color4)
draw_set_font(font_dialog);
draw_text_color(32,32,initiative_string, c_white, c_white, c_white, c_white, 1.0)

for (var i = 0; i < 9; ++i)
{
	var enemy = ""
	if (ds_list_size(enemies_copies) > i)
	{
		enemy = enemies_copies[|i];	
	}
	if (enemy == "" or not battle_is_alive(enemy))
	{
		continue;	
	}
	
	var xx = battle_get_enemy_tile_x(i)
	var yy = battle_get_enemy_tile_y(i)
	var highlight = c_gray;
	if (initiative[| initiative_index] == enemy)
	{
		highlight = c_green;
	}
	
	var hp_string = "";
	if (stat_has_tag(initiative[| initiative_index], "AUTO_SCAN"))
	{
		hp_string = stat_get(enemy, "HP");	
	}
	battle_draw_tile(xx, yy, enemy_tile_width, enemy_tile_height, highlight, enemy, hp_string);
}



for (var i = 1; i <= number_of_characters_in_party(); ++i)
{
	var pc = "PC"+string(i)+"Copy";
	var hp_string = string(stat_get(pc, "HP"));
	var xx = battle_get_pc_tile_x(i);
	var yy = battle_get_pc_tile_y(i);
	var highlight = c_gray;
	if (initiative[| initiative_index] == pc)
	{
		highlight = c_green;
	}
	
	battle_draw_tile(xx, yy, pc_tile_width, pc_tile_height, highlight, pc, hp_string);
}

ds_list_destroy(turn_left_initiative)