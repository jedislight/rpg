/// @description Insert description here
// You can write your code in this editor
var party_pawn = instance_find(PartyPawn, 0);
var party_moving = false;
if (instance_exists(party_pawn))
{
	party_moving = party_pawn.moving;	
}
if (not combatants_added and not party_moving)
{
	for (var i = 1; i <= number_of_characters_in_party(); ++i)
	{
		pc = "PC"+string(i);
		stat_copy(pc, pc+"Copy")
		ds_list_add(initiative, pc+"Copy")
	}

	for( var i = 0; i < ds_list_size(enemies); ++i)
	{
		var enemy = enemies[|i];
		ds_list_add(enemies_copies, enemy);
		if (enemy = "")
		{
			continue;	
		}
		var enemy_copy = enemy + string(i)
		stat_copy(enemy, enemy_copy)
		ds_list_add(initiative, enemy_copy);
		ds_list_replace(enemies_copies, i, enemy_copy);
	}
	
	combatants_added = true;
}

if( combatants_added and not battle_over)
{
	if (not battle_is_visual_playing())
	{
		if (turn_taken)
		{
			turn_taken = false;
			++initiative_index;
			turn_started = false;
		}
		if (not turn_started and initiative_index == 0 and not turn_taken)
		{
			++turn;
			show_debug_message("Battle begin turn " + string(turn))
			battle_initiative_sort();
			var init_order_log = "Initiative order:";
			for(var i = 0; i < ds_list_size(initiative); ++i)
			{
				init_order_log += " " + initiative[|i] + " ";	
			}
			show_debug_message(init_order_log);
			turn_started = true;
		}
		if (not turn_taken)
		{
			if(initiative_index < ds_list_size(initiative))
			{
				if(not battle_pcs_defeated() and not battle_enemies_defeated())
				{
					var combatant = initiative[|initiative_index];
					if (battle_combatant_can_take_turn(combatant))
					{
						if(battle_combatant_turn(combatant))
						{
							turn_taken = true;
						}
					}
					else
					{
						turn_taken = true;
					}
				}
				else
				{
					turn_taken = true;
				}
			}
			else
			{
				initiative_index = 0;	
				show_debug_message("Battle end turn " + string(turn))
			}
		}
	}
}

if (combatants_added and (battle_pcs_defeated() or battle_enemies_defeated()))
{
	battle_over = true;
}

if (combatants_added and (battle_over and not battle_is_visual_playing()))
{
	var xp_reward = 0;
	var style_reward = "";
	if (not battle_no_rewards)
	{
		if (battle_enemies_defeated() and not battle_pcs_defeated())
		{
			var gold_reward = 0;
			for(var i = 0; i < ds_list_size(initiative); ++i)
			{
				var combatant = initiative[|i];
				if (not battle_is_pc(combatant))
				{
					xp_reward += stat_get(combatant, "XP");
					gold_reward += stat_get(combatant, "GOLD");
					if(stat_has(combatant, "STYLE") and not stat_has_tag(combatant, "NO_STYLE_LEARNING"))
					{
						var style = stat_get(combatant, "STYLE");
						if (ds_list_find_index(Data.known_styles, style) < 0)
						{
							style_reward = style;	
						}
					}
				}
			}
	
			var dw = instance_create_layer(0,0,"Instances_Upper", DialogWindow)
			dw.text = "Battle Won!\nFound " + string(gold_reward) + " Gold\nGained " + string(xp_reward) + "XP";
			if (style_reward != "")
			{
				dw.text += "\n\n Learned " + style_reward + "!";
				ds_list_add(Data.known_styles, style_reward);
			}
			Data.gold += gold_reward;
		}
	}

	for (var i = 1; i <= number_of_characters_in_party(); ++i)
	{
		
		var pc = "PC"+string(i);
		stat_set(pc, "HP", stat_get(pc+"Copy", "HP"));
		stat_set(pc, "RESERVED_HP", stat_get(pc+"Copy", "RESERVED_HP"));
		
		if (not battle_no_rewards)
		{
			if ( battle_is_alive(pc))
			{
				stat_adjust(pc, "XP", xp_reward)
				if(stat_ready_to_level(pc))
				{
					stat_level_up(pc)
				}
			}
		}
	}

	for (var i = 0; i < ds_list_size(initiative); ++i)
	{
		stat_clear(initiative[|i])
	}
	
	with(BattleActionMenu) { instance_destroy()}
	instance_destroy()
}
