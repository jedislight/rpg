/// @description Insert description here
// You can write your code in this editor
event_inherited();

if (keyboard_check_direct(vk_control) and keyboard_check_pressed(ord("L")))
{
	stat_level_up("PC1")	
	stat_level_up("PC2")	
	stat_level_up("PC3")	
	stat_level_up("PC4")	
	
	if (not keyboard_check_direct(vk_shift))
	{
		with(DialogWindow)
		{
			instance_destroy();	
		}
	}
}

if (keyboard_check_direct(vk_control) and keyboard_check_pressed(ord("S")))
{
	for (var i = ds_map_find_first(Data.stats); not is_undefined(i); i = ds_map_find_next(Data.stats, i))
	{
		if (string_length(i) > 5 and string_copy(i, 1, 5) == "STYLE")
		{
			var index = ds_list_find_index(Data.known_styles, i);
			if (index < 0)
			{
				ds_list_add(Data.known_styles, i);	
			}
		}
	}
}

if (keyboard_check_direct(vk_control) and keyboard_check_pressed(ord("D")))
{
	with(PartyPawn){instance_destroy()}
	room_goto(DebugIsland);
}