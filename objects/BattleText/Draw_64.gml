/// @description Insert description here
// You can write your code in this editor
event_inherited();

var center = 256
var width = string_width(text)
var left = center - width div 2
var right = center + width div 2
var top = 0;
var bottom = 32;

draw_set_font(font_dialog);
draw_set_halign(fa_center);
draw_rectangle_color_analagous_outline(left, top, right, bottom, c_blue, c_blue, c_blue, c_blue)
draw_text_color(center, top, text, c_white, c_white, c_white, c_white, 1.0)
draw_set_halign(fa_left);