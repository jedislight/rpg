/// @description Insert description here
// You can write your code in this editor


if (stat_has(item, "ICON"))
{
	image_alpha = 0.25;
	draw_self();
	draw_sprite(stat_get(item, "ICON"), 0, x, y);
}
else
{
	draw_self();
}