/// @description Insert description here
// You can write your code in this editor
event_inherited();

if (closing)
{
	frame -=10;
}
else
{
	frame += 10;
}
frame = clamp(frame, 0, 256);

if (closing and frame == 0)
{
	instance_destroy()	
}