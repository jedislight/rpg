/// @description Insert description here
// You can write your code in this editor
event_inherited();

draw_rectangle_color_analagous_outline(max(32, 256-frame), max(32, 256-frame), min(512-32, 256+frame), min(512-32,256+frame), c_blue, c_blue, c_blue, c_blue);

if (frame >= 256)
{
	draw_set_font(font_dialog);
	draw_text_ext_color(32,32, text, font_get_size(font_dialog), 512-64, c_white, c_white, c_white, c_white, 1.0);
	
	var icon = stat_get(pc, "ICON");
	draw_sprite(icon, 0 , 512-32-sprite_get_width(icon), 32);
}