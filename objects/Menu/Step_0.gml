/// @description Insert description here
// You can write your code in this editor
event_inherited();
if (frame >= 256 and not closing and not input_lock_check(object_get_name(DialogWindow)))
{
	if (input_get_press("CANCEL"))
	{
		closing = true;	
		audio_play_effect(sound_negative_2);
		exit;
	}
	
	if (input_get_press("OKAY"))
	{
		if (cursor_index == 0)
		{
			// player stuff
			var character = "PC"  + string(character_index);
			var available_styles = ds_list_create();
			ds_list_copy(available_styles, Data.known_styles);
			for(var i = 1; i <= 4; ++i)
			{
				if (i != character_index)
				{
					var unique_style = Data.unique_styles[|i];
					ds_list_remove_by_value(available_styles, unique_style)
				}
			}
			var style_index = ds_list_find_index(available_styles, stat_get(character, "STYLE"));
			style_index += 1;
			if (style_index >= ds_list_size(available_styles))
			{
				style_index = 0;	
			}
			var new_style = available_styles[|style_index];
			stat_apply_style(character, new_style);
			stat_unequip_all(character);
			ds_list_destroy(available_styles);
		}
		else
		{
			// item stuff
			var item_index = 0;
			for (var i = ds_map_find_first(Data.key_items); not is_undefined(i); i = ds_map_find_next(Data.key_items, i))
			{
				item_index += 1;
				
				if (item_index == cursor_index)
				{
					if (stat_exists(i) and stat_has_tag(i, "CONSUMABLE") and stat_has_tag(i, "MENU"))
					{
						var character = "PC"  + string(character_index);
						item_apply(character, i);
						break;	
					}
					
					if (stat_exists(i) and stat_has_tag(i, "EQUIPMENT"))
					{
						var character = "PC"  + string(character_index);
						if(stat_is_equipped(character, i))
						{
							// unequip
							stat_equip(character, i);	
						}
						else
						{
							// equip
							if (party_equipped_count(i) < Data.key_items[? i] and stat_can_equip(character, i))
							{
								stat_equip(character, i);
							}
							else
							{
								audio_play_effect(sound_negative);
							}
						}
					}
				}
			}
		}
	}

	if (input_get_press("RIGHT"))
	{
		character_index += 1;
		audio_play_effect(sound_interface2);
	}

	if (input_get_press("LEFT"))
	{
		character_index -= 1;
		audio_play_effect(sound_interface2);
	}

	if (input_get_press("UP"))
	{
		cursor_index -= 1;
		audio_play_effect(sound_interface2);
	}

	if (input_get_press("DOWN"))
	{
		cursor_index += 1;
		audio_play_effect(sound_interface2);
	}

	var characters_in_party = number_of_characters_in_party();
	
	if (character_index <= 0)
	{
		character_index = characters_in_party;	
	}

	if (character_index > characters_in_party)
	{
		character_index = 1;
	}

	var cursor_index_max = ds_map_size(Data.key_items);
	if (cursor_index < 0)
	{
		cursor_index = cursor_index_max;	
	}

	if (cursor_index > cursor_index_max)
	{
		cursor_index = 0;
	}
}
pc = "PC"  + string(character_index);

text = " " + pc;
if (cursor_index == 0)
{
	text = ">" + pc	
}
text += "\n HP: " + string(stat_get(pc, "HP")) + " / " + string(stat_get(pc, "HP_MAX"));
text += "\n"
text += "\n ATK: " + string(stat_get(pc, "ATK"))
text += "\n DEF: " + string(stat_get(pc, "DEF"))
text += "\n MAG: " + string(stat_get(pc, "MAG"))
text += "\n RES: " + string(stat_get(pc, "RES"))
text += "\n SPD: " + string(stat_get(pc, "SPD"))
text += "\n VIT: " + string(stat_get(pc, "VIT"))
text += "\n EVADE: " + string(stat_get(pc, "EVADE"))
text += "\n"
text += "\n TAGS: " + stat_get(pc, "TAGS");
text += "\n"
text += "\n STYLE: " + string(stat_get(pc, "STYLE"))
text += "\n STYLE_TAGS: " + stat_get(stat_get(pc, "STYLE"), "TAGS")
text += "\n XP: " + string(stat_get(pc, "XP")) + " / " + string(stat_get(pc, "TOLEVEL"));
text += "\n"
text += "\n"
text += "\n Gold: " + string(Data.gold);
text += "\n Items:"
var item_index = 0;
for (var i = ds_map_find_first(Data.key_items); not is_undefined(i); i = ds_map_find_next(Data.key_items, i))
{
	item_index += 1;
	
	var count = Data.key_items[? i];
	if (count)
	{
		if (stat_is_equipped(pc, i))
		{
			text += "\n E ";	
		}
		else
		{
			text += "\n   ";
		}
		if (item_index == cursor_index)
		{
			text += ">"
		}
		else
		{
			text += " ";
		}
		if (count > 1)
		{
			text += string(count)+"x "	
		}
		else
		{
			text += "   "
		}
		
		text += string(i);
		
		if (item_index == cursor_index)
		{
			if (stat_exists(string(i)) and stat_has(string(i), "DESCRIPTION"))
			{
				text += " - " + stat_get(string(i), "DESCRIPTION");	
			}
		}
		
	}	
}
