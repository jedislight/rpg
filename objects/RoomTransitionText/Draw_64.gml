/// @description Insert description here
// You can write your code in this editor
event_inherited();

draw_set_font(font_room_transistion);
draw_set_valign(fa_middle)
draw_set_halign(fa_center)

var alpha = image_alpha;
if (alpha > 1.65)
{
	alpha = 2.0 - alpha	
}

draw_text_ext_transformed_color(256+3,256+2,text,font_get_size(draw_get_font()),512,1.0,1.0,0.0,c_black,c_black,c_black,c_black, clamp(alpha,0,0.65));
draw_text_ext_transformed_color(256-2,256-3,text,font_get_size(draw_get_font()),512,1.0,1.0,0.0,c_black,c_black,c_black,c_black, clamp(alpha,0,0.65));
draw_text_ext_transformed_color(256,256,text,font_get_size(draw_get_font()),512,1.0,1.0,0.0,c_white,c_white,c_white,c_white, alpha);


draw_set_halign(fa_left)
draw_set_valign(fa_top)