/// @description Insert description here
// You can write your code in this editor

var any_alive = false;
for (var i = ds_map_find_first(Data.stats); not is_undefined(i) ; i = ds_map_find_next(Data.stats, i))
{
	if (battle_is_pc(i))
	{
		if (battle_is_alive(i))
		{
			any_alive = true;
			break;
		}
	}
}

if( not any_alive)
{
	data_load("auto");
}