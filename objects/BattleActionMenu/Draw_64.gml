/// @description Insert description here
// You can write your code in this editor
event_inherited();

if (is_enemy)
{
	exit;	
}

draw_rectangle_color_analagous_outline(32, 512-32, 512-32, 512, c_blue, c_blue, c_blue, c_blue)
draw_set_font(font_dialog);
var text = ""
var description = "";
var selection = options[|index];
if (stat_exists(selection) and stat_has(selection, "DESCRIPTION"))
{
	description = stat_get(selection, "DESCRIPTION");	
}
for (var i = 0; i < ds_list_size(options); ++i)
{
	if (index == i)
	{
		text += " >"	
	}
	else
	{
		text += "  "
	}
	
	var action = options[| i];
	if (string_char_at(action, 1) == "!")
	{	
		action = string_copy(action, 2, string_length(action)-1);
	}
	text += action
	
	if (index == i)
	{
		text += "< "	
	}
	else
	{
		text += "  "
	}
}

draw_text_color(32, 512-32, text, c_white, c_white, c_white, c_white, 1.0)
draw_text_color(32, 512-16, description, c_white, c_white, c_white, c_white, 1.0)