/// @description Insert description here
// You can write your code in this editor
event_inherited();

while(!ds_stack_empty(options_stack))
{
	ds_list_destroy(ds_stack_pop(options_stack));
}

ds_stack_destroy(options_stack);

show_debug_message("Battle Action Menu Cleanup for:" + owner);