/// @description Insert description here
// You can write your code in this editor
event_inherited();

if (instance_number(Battle) == 0)
{
	instance_destroy();
	exit;
}

is_enemy = not battle_is_pc(owner);
if(done)
{
	manual_targeting = false;	
}

if (first_step)
{
	var actions = stat_get_actions(owner);
	ds_list_copy(options, actions);
	ds_list_destroy(actions);
	first_step = false;
}
if(battle_is_visual_playing())
{
	exit;	
}

if(instance_number(Battle) == 0 or Battle.battle_over)
{
	instance_destroy();
	exit;	
}
if ( manual_targeting == false)
{
	var previous = index;
	if (input_get_press("LEFT"))
	{
		index -= 1;
	}
	if (input_get_press("RIGHT"))
	{
		index += 1;
	}

	index = clamp(index, 0, ds_list_size(options)-1);
	if(index != previous)
	{
		audio_play_effect(sound_click);
	}
}
else
{
	if (input_get_press("CANCEL"))
	{
		manual_targeting = false;
		Battle.manual_target = "";
		audio_play_effect(sound_negative);
		exit;
	}
	else
	{
		battle_manual_target_step();
	}
}

var confirm = (not is_enemy and input_get_press("OKAY")) or (is_enemy);
if (done)
{
	confirm = false;	
}
if (is_enemy and not manual_targeting)
{
	index = irandom_range(0, ds_list_size(options)-1);	
}
var attack_index = ds_list_find_index(options, "Attack");
if(stat_has_tag(owner, "AUTO_ATTACK") and attack_index >= 0 and not done)
{
	confirm = true;
	index = attack_index;
}

var harm_target = is_enemy;
var help_target = not harm_target;

if (confirm)
{
	for (var i = ds_map_find_first(Data.stats); not is_undefined(i); i = ds_map_find_next(Data.stats, i))
	{
		if (i == options[|index])
		{
			if ( stat_has_tag(i, "CONSUMABLE") and stat_has_tag(i, "BATTLE"))
			{
				var item_name = i;
				item_apply(owner, item_name);
				battle_create_text(owner + " uses " + item_name)
				done = true;
				exit;
			}
		}
	}
	
	switch(options[|index])
	{
		case "!ATTACK":
			if (stat_has_tag(owner, "NO_ATTACK_TARGET"))
			{
				battle_random_attack(owner);
				done=true;
				exit;
			}
			if (not manual_targeting)
			{
				manual_targeting = true;
				manual_targeting_party = harm_target;
			}
			else
			{
				if (Battle.manual_target != "" and not is_undefined(Battle.manual_target) and battle_is_alive(Battle.manual_target))
				{
					battle_attack(owner, Battle.manual_target);
					battle_create_text(owner + " attacks " + Battle.manual_target)
					done = true;
				}
			}
			break;
			
		case "!FLEE":
			var success = battle_try_flee(owner);
			if(not success)
			{
				battle_create_text(owner + " could not flee!")
			}
			done = true;
			break;
			
		case "!ITEM":
			var battle_consumables = ds_list_create();
			for (var i = ds_map_find_first(Data.key_items); not is_undefined(i); i = ds_map_find_next(Data.key_items, i))
			{
				var count = Data.key_items[? i];
				if (count <= 0)
				{
					continue;	
				}
				
				if(not stat_has_tag(i, "BATTLE") or not stat_has_tag(i, "CONSUMABLE"))
				{
					continue;
				}
				
				ds_list_add(battle_consumables, i);
			}
			
			if (ds_list_size(battle_consumables) == 0)
			{
				audio_play_effect(sound_negative)
				ds_list_destroy(battle_consumables)
				break;	
			}
			
			ds_stack_push(options_stack, battle_consumables);
			options = battle_consumables;
			break;
			
		case "!HEAL":
			if (not manual_targeting)
			{
				manual_targeting = true;
				manual_targeting_party = help_target;
				show_debug_message("HEAL target type:" + string(manual_targeting_party));
			}
			else
			{
				if (instance_number(Battle) > 0 and Battle.manual_target != "" and battle_is_alive(Battle.manual_target))
				{
					battle_heal(owner, Battle.manual_target);
					battle_create_text(owner + " heals " + Battle.manual_target)
					stat_reserve_hp(owner, 0.05);
					done = true;
				}
			}
			break;
			
		case "!REPAIR":
			battle_heal(owner, owner);
			battle_create_text(owner + " repairs themself");
			stat_reserve_hp(owner, 0.05);
			done = true;
			break;
		
		case "!SPIRIT_BURST":
			for(var i = 0; i < ds_list_size(Battle.initiative); ++i)
			{
				var combatant = Battle.initiative[|i];
				if(battle_is_alive(combatant) and combatant != owner)
				{
					battle_heal(owner, combatant);
					if(not stat_has(combatant, "STYLE") or stat_get(combatant, "STYLE") != "STYLE_SPIRIT")
					{
						stat_apply_style(combatant, "STYLE_SPIRIT");
						stat_add_tag(combatant, "NO_STYLE_LEARNING");
						text_flyout_create(combatant, "New Style: Spirit" , c_white)
					}
				}
			}
			stat_reserve_hp(owner, 0.25);
			battle_create_text(owner + " eminates spirit energy");
			done = true;
			break;
			
		case "!PRIMAL_BURST":
			for(var i = 0; i < ds_list_size(Battle.initiative); ++i)
			{
				var combatant = Battle.initiative[|i];
				if(battle_is_alive(combatant) and combatant != owner)
				{
					var atk_cache = stat_get(owner, "ATK");
					stat_set(owner, "ATK", 5);
					battle_attack(owner, combatant);
					stat_set(owner, "ATK", atk_cache);
					if(not stat_has(combatant, "STYLE") or stat_get(combatant, "STYLE") != "STYLE_BERZERKER")
					{
						stat_apply_style(combatant, "STYLE_BERZERKER");
						stat_add_tag(combatant, "NO_STYLE_LEARNING");
						text_flyout_create(combatant, "New Style: Primal" , c_white)
					}
				}
			}
			stat_reserve_hp(owner, 0.25);
			battle_create_text(owner + " eminates primal energy");
			done = true;
			break;
			break;
		case "!SCAN":
			if(stat_has_tag(owner, "AUTO_SCAN"))
			{		
				for(var i = 0; i < ds_list_size(Battle.initiative); ++i)
				{
					var combatant = Battle.initiative[|i];
					if(is_enemy == battle_is_pc(combatant) and battle_is_alive(combatant))
					{
						stat_adjust(combatant, "ATK", -round(stat_get(combatant, "ATK")*0.1))	
						stat_adjust(combatant, "DEF", -round(stat_get(combatant, "DEF")*0.1))
						text_flyout_create(combatant, "-10% ATK\n-10% DEF", c_red);
						battle_create_text(owner + " scans for weaknesses");
					}
				}
				done = true;
			}
			else
			{
				var tags = stat_get(owner, "TAGS");
				tags += ";AUTO_SCAN";
				stat_set(owner, "TAGS", tags);
				battle_create_text(owner + " enables scanners");
				done = true;
			}
			break;
		
		default: show_debug_message("UNKNOWN BATTLE ACTION: " + options[| index])
	}
}
else if(input_get_press("CANCEL"))
{
	if (ds_stack_size(options_stack) > 1)
	{
		ds_list_destroy(ds_stack_pop(options_stack));
		options = ds_stack_top(options_stack);
		audio_play_effect(sound_negative);
	}
}