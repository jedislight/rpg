/// @description Insert description here
// You can write your code in this editor
event_inherited();

ps = part_system_create_layer("Instances_Upper", false)

emitter = part_emitter_create(ps)

part_emitter_region(ps, emitter, x+16,x+18,y+10,y+12,ps_shape_rectangle, ps_distr_gaussian)

smoke_part = part_type_create();
part_type_sprite(smoke_part , sprite321, false, false, false);
part_type_size(smoke_part, 0.01, 0.8, 0.05, 0.1);
part_type_scale(smoke_part, 0.5, 0.5);
part_type_alpha3(smoke_part,1, 0.50, 0.0);
part_type_speed(smoke_part, 0, 0, .02, .02);
part_type_direction(smoke_part, 70, 120, 1, 1);
part_type_gravity(smoke_part, .05, 90);
part_type_life(smoke_part, 15, 45);

part_emitter_stream(ps, emitter, smoke_part, -2)