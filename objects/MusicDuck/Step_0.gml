/// @description Insert description here
// You can write your code in this editor
event_inherited();

for (var i = 0; i < ds_list_size(wait_list); ++i)
{
	if(audio_is_playing(wait_list[|i]))
	{
		//DUCK
		audio_sound_gain(asset_get_index(global.playing_music), 0.75, 50);
		break;
	}
	else
	{
		ds_list_delete(wait_list, i);
		if (ds_list_empty(wait_list))
		{
			audio_sound_gain(asset_get_index(global.playing_music), 1.0, 50);
			//UNDUCK	
		}
		exit;	
	}
}