/// @description Insert description here
// You can write your code in this editor

randomize();
instance_create_depth(0,0,0, Data);
for (var i = 0; i < 256; ++i)
{
	global.terrain_walkable[i] = false;	
}
global.terrain_walkable[2] = true; // grass
global.terrain_walkable[3] = true; // stone
global.terrain_walkable[4] = true; // dirt
global.terrain_walkable[38] = true; // ship-floor
global.terrain_walkable[83] = true; // forest
global.terrain_walkable[88] = true; // flower
global.terrain_walkable[87] = true; // flower2
global.terrain_walkable[65] = true; // rough sand
global.terrain_walkable[96] = true; // smooth stone
global.terrain_walkable[97] = true; // rough stone

global.previous_room = noone

global.input_locks = ds_map_create()

global.text_rate = 1

global.playing_music = noone;
global.music_stack = ds_stack_create();

instance_create_depth(0,0,0, Party);
instance_create_depth(0,0,0, MusicDuck);

if(debug_mode)
{
	instance_create_depth(0,0,0, DebugFunctions);	
	instance_create_depth(0,0,0, DebugConsole);
	
}

room_goto(SpaceShipIntro);

if(not directory_exists("saves"))
{
	directory_create("saves")
}
