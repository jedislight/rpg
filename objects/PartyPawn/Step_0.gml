/// @description Insert description here
// You can write your code in this editor
if (moving)
{
	if (x != target_x || y != target_y)
	{
		x += 4 * sign(target_x-x)
		y += 4 * sign(target_y-y)
	}
	else
	{
		moving = false
		
		mask_index = no_collision
	}
}
else if(moving_cooldown_frame)
{
	moving_cooldown_frame = false
}
else
{
	var mov_x = 0;
	var mov_y = 0;
	if (input_get_held("DOWN"))
	{
		mov_x = 0;
		mov_y = 32;
	}
	if (input_get_held("UP"))
	{
		mov_x = 0;
		mov_y = -32;
	}
	if (input_get_held("LEFT"))
	{
		mov_x = -32;
		mov_y = 0;
	}
	if (input_get_held("RIGHT"))
	{
		mov_x = 32;
		mov_y = 0;
	}
	
	if (input_lock_check())
	{
		mov_x = 0;
		mov_y = 0;
	}
	else
	{
		if (input_get_press("CANCEL"))
		{
			menu_open();	
		}
	}
	
	if (mov_x != 0 || mov_y != 0)
	{
		var tile_index = tile_get_index_from_layer_pos("Terrain", x+mov_x, y+mov_y)
		var zone_index = tile_get_index_from_layer_pos("Zones", x+mov_x, y+mov_y)
		var walkable = global.terrain_walkable[tile_index];
		show_debug_message("attempting to move into terrain type: " + string(tile_index) + " walkable: " + string(walkable) + " zone: " + string(zone_index))
		if(walkable)
		{
			target_x = x + mov_x;
			target_y = y + mov_y;
			moving = true;
			moving_cooldown_frame = true
			mask_index = sprite_index;
			
			var zone_list = Data.zones[| zone_index];
			ds_list_shuffle(zone_list);
			var encounter_list_string = zone_list[|0];
			
			if (not is_undefined(encounter_list_string))
			{
				++Data.random_encounter_accumulator;
				show_debug_message(string(Data.random_encounter_accumulator))
				if (Data.random_encounter_accumulator > random_encounter_step_count)
				{
					random_encounter_step_count = irandom_range(20,40);
					Data.random_encounter_accumulator = 0;
				
					var battle = instance_create_layer(0,0, "Instances", Battle);
					var encounter_list = ds_list_from_semicolon_delimited_string(encounter_list_string);
					ds_list_copy(battle.enemies, encounter_list);
					ds_list_destroy(encounter_list);
				}
			}
			

		}
	}
}