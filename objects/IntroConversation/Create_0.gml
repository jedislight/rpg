/// @description Insert description here
// You can write your code in this editor
event_inherited();

conversation_add("PC2", "You seem to be preparing your drop pod incorrectly. Standard protocols dictate flight preparations should be done after loading to account for proper weight calibration.");
conversation_add("PC1", "Sorry Ship. I've been stuck in this tin can for too long. I just want to get planetside as soon as possible. Ground crew gigs with a 6 year transit are a pain in my ass.");
conversation_add("PC2", "I am not made of tin.");
conversation_add("PC1", "What?");
conversation_add("PC2", "I am not made of tin.");
conversation_add("PC1", "Are you alright Ship? Your humor isn't usually this ... bad.");
conversation_add("PC2", "My apologies. I am concurrently trying to guide two dozen emergency drop pod flight preparations, keep the crew safe from decompression, and maintain calm.");
conversation_add("PC1", "Emergency?! We've been planning this search and rescue drop for years Ship! What the hell is going on!?");
conversation_add("PC2", "It appears we have been boarded by unidentified mechanical assailants. 32% of my physical systems are offline or compromised. Confirmed crew casualties 65%.");
conversation_add("PC1", "WHAT! Why are you only telling me this NOW!");
conversation_add("PC2", "You were already preparing your flight systems. Calmly.");
conversation_add("PC1", "...");
conversation_add("PC1", "Fuck you, Ship. How long do we have until -");
conversation_event(IntroExplosion);
conversation_event(GotoOverworld);
