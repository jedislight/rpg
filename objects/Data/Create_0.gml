/// @description Insert description here
// You can write your code in this editor

key_items = ds_map_create()


opened_chests = ds_list_create()

gold = 50

Data.stats = ds_map_create()

Data.zones = ds_list_create()

Data.random_encounter_accumulator = 0;

Data.known_styles = ds_list_create();

for(var i = 0; i < 9; ++i)
{
	ds_list_add(Data.zones, ds_list_create());	
}

// STYLES
stat_set("STYLE_EXPLORER", "VIT", 3)
stat_set("STYLE_EXPLORER", "ATK", 2)
stat_set("STYLE_EXPLORER", "DEF", 2)
stat_set("STYLE_EXPLORER", "MAG", 2)
stat_set("STYLE_EXPLORER", "RES", 2)
stat_set("STYLE_EXPLORER", "SPD", 4)
stat_set("STYLE_EXPLORER", "EVADE", 10)
stat_set("STYLE_EXPLORER", "TAGS", "!ATTACK;EQUIPMENT;WEAPON;ARMOR;!ITEM;+!ITEM;!FLEE;+!FLEE")
ds_list_add(Data.known_styles, "STYLE_EXPLORER");

stat_set("STYLE_AI", "VIT", 4)
stat_set("STYLE_AI", "ATK", 2)
stat_set("STYLE_AI", "DEF", 4)
stat_set("STYLE_AI", "MAG", 2)
stat_set("STYLE_AI", "RES", 3)
stat_set("STYLE_AI", "SPD", 1)
stat_set("STYLE_AI", "EVADE", 5)
stat_set("STYLE_AI", "TAGS", "!ATTACK;EQUIPMENT;WEAPON;AUTO_SCAN;+!SCAN;+REPAIR")
ds_list_add(Data.known_styles, "STYLE_AI");

stat_set("STYLE_BERZERKER", "VIT", 2)
stat_set("STYLE_BERZERKER", "ATK", 4)
stat_set("STYLE_BERZERKER", "DEF", 2)
stat_set("STYLE_BERZERKER", "SPD", 3)
stat_set("STYLE_BERZERKER", "MAG", 1)
stat_set("STYLE_BERZERKER", "RES", 1)
stat_set("STYLE_BERZERKER", "EVADE", 10)
stat_set("STYLE_BERZERKER", "TAGS", "!ATTACK;EQUIPMENT;WEAPON;HEAVY_WEAPON;+WEAPON;+HEAVY_WEAPON;+!PRIMAL_BURST")

stat_set("STYLE_PHASE", "VIT", 2)
stat_set("STYLE_PHASE", "ATK", 3)
stat_set("STYLE_PHASE", "DEF", 1)
stat_set("STYLE_PHASE", "SPD", 5)
stat_set("STYLE_PHASE", "MAG", 2)
stat_set("STYLE_PHASE", "RES", 3)
stat_set("STYLE_PHASE", "EVADE", 50)
stat_set("STYLE_PHASE", "TAGS", "!ATTACK;EQUIPMENT;WEAPON")

stat_set("STYLE_SPIRIT", "VIT", 2)
stat_set("STYLE_SPIRIT", "ATK", 1)
stat_set("STYLE_SPIRIT", "DEF", 2)
stat_set("STYLE_SPIRIT", "SPD", 1)
stat_set("STYLE_SPIRIT", "MAG", 3)
stat_set("STYLE_SPIRIT", "RES", 4)
stat_set("STYLE_SPIRIT", "EVADE", 5)
stat_set("STYLE_SPIRIT", "TAGS", "!HEAL;+!HEAL;+!SPIRIT_BURST")

// ENEMIES
stat_set("Salamander", "VIT", 20)
stat_set("Salamander", "ATK", 5)
stat_set("Salamander", "DEF", 1)
stat_set("Salamander", "SPD", 15)
stat_set("Salamander", "MAG", 5)
stat_set("Salamander", "RES", 5)
stat_set("Salamander", "EVADE", 10)
stat_set("Salamander", "XP", 3)
stat_set("Salamander", "GOLD", 7)
stat_set("Salamander", "ICON", sprite10);
stat_set("Salamander", "ATTACK_SOUND", sound_slime1);
stat_set("Salamander", "TAGS", "!ATTACK")

stat_set("Falcon", "VIT", 10)
stat_set("Falcon", "ATK", 2)
stat_set("Falcon", "DEF", 10)
stat_set("Falcon", "SPD", 15)
stat_set("Falcon", "MAG", 5)
stat_set("Falcon", "RES", 25)
stat_set("Falcon", "EVADE", 10)
stat_set("Falcon", "XP", 4)
stat_set("Falcon", "GOLD", 7)
stat_set("Falcon", "ICON", sprite10111);
stat_set("Falcon", "ATTACK_SOUND", sound_swing2);
stat_set("Falcon", "TAGS", "!ATTACK")

stat_copy("Falcon", "Spirit Falcon");
stat_apply_style("Spirit Falcon", "STYLE_SPIRIT");
stat_set("Spirit Falcon", "XP", 7)
stat_set("Spirit Falcon", "GOLD", 7)
stat_set("Spirit Falcon", "ICON", sprite101111);

stat_set("Lion", "VIT", 30)
stat_set("Lion", "ATK", 10)
stat_set("Lion", "DEF", 15)
stat_set("Lion", "SPD", 25)
stat_set("Lion", "MAG", 10)
stat_set("Lion", "RES", 15)
stat_set("Lion", "EVADE", 10)
stat_set("Lion", "XP", 10)
stat_set("Lion", "GOLD", 15)
stat_set("Lion", "ICON", sprite101);
stat_set("Lion", "ATTACK_SOUND", sound_swing3);
stat_set("Lion", "TAGS", "!ATTACK")

stat_copy("Lion", "Phase Lion");
stat_apply_style("Phase Lion", "STYLE_PHASE");
stat_set("Phase Lion", "XP", 100)
stat_set("Phase Lion", "GOLD", 100)
stat_set("Phase Lion", "ICON", sprite1011);
stat_set("Phase Lion", "ATTACK_SOUND", sound_mnstr12);

stat_set("Turtle", "VIT", 5)
stat_set("Turtle", "ATK", 4)
stat_set("Turtle", "DEF", 512)
stat_set("Turtle", "SPD", 5)
stat_set("Turtle", "MAG", 4)
stat_set("Turtle", "RES", 1)
stat_set("Turtle", "EVADE", 0)
stat_set("Turtle", "XP", 4)
stat_set("Turtle", "GOLD", 10)
stat_set("Turtle", "ICON", sprite103);
stat_set("Turtle", "ATTACK_SOUND", sound_mnstr10);
stat_set("Turtle", "TAGS", "!ATTACK")

stat_set("Hare", "VIT", 2)
stat_set("Hare", "ATK", 2)
stat_set("Hare", "DEF", 2)
stat_set("Hare", "SPD", 15)
stat_set("Hare", "MAG", 5)
stat_set("Hare", "RES", 5)
stat_set("Hare", "EVADE", 25)
stat_set("Hare", "XP", 2)
stat_set("Hare", "GOLD", 4)
stat_set("Hare", "ICON", sprite24);
stat_set("Hare", "ATTACK_SOUND", sound_mnstr10);
stat_set("Hare", "TAGS", "!ATTACK")

stat_copy("Hare", "Spirit Hare");
stat_apply_style("Spirit Hare", "STYLE_SPIRIT");
stat_set("Spirit Hare", "XP", 5)
stat_set("Spirit Hare", "GOLD", 7)
stat_set("Spirit Hare", "ICON", sprite241);

stat_set("Spirit", "VIT", 1)
stat_set("Spirit", "ATK", 2)
stat_set("Spirit", "DEF", 2)
stat_set("Spirit", "SPD", 20)
stat_set("Spirit", "MAG", 2)
stat_set("Spirit", "RES", 2)
stat_set("Spirit", "EVADE", 5)
stat_set("Spirit", "XP", 1)
stat_set("Spirit", "GOLD", 1)
stat_set("Spirit", "ICON", sprite1031);
stat_set("Spirit", "ATTACK_SOUND", sound_magic1);
stat_set("Spirit", "TAGS", "NO_STYLE_LEARNING")
stat_apply_style("Spirit", "STYLE_SPIRIT")
repeat(3) stat_level_up("Spirit")

stat_set("Primal Spirit", "VIT", 1)
stat_set("Primal Spirit", "ATK", 2)
stat_set("Primal Spirit", "DEF", 2)
stat_set("Primal Spirit", "SPD", 20)
stat_set("Primal Spirit", "MAG", 2)
stat_set("Primal Spirit", "RES", 2)
stat_set("Primal Spirit", "EVADE", 5)
stat_set("Primal Spirit", "XP", 15)
stat_set("Primal Spirit", "GOLD", 1)
stat_set("Primal Spirit", "ICON", sprite1031);
stat_set("Primal Spirit", "ATTACK_SOUND", sound_mnstr10);
stat_set("Primal Spirit", "TAGS", "NO_STYLE_LEARNING")
stat_apply_style("Primal Spirit", "STYLE_BERZERKER")
repeat(3) stat_level_up("Primal Spirit")

// ITEMS
stat_set("Might Stone", "ATK", 50);
stat_set("Might Stone", "TAGS", "CONSUMABLE;BATTLE");
stat_set("Might Stone", "PRICE", 100);
stat_set("Might Stone", "DESCRIPTION", "+50 ATK during battle");

stat_set("Bandages", "HP", 15);
stat_set("Bandages", "TAGS", "CONSUMABLE;MENU");
stat_set("Bandages", "PRICE", 5);
stat_set("Bandages", "DESCRIPTION", "Restores 15 HP");

stat_set("Potion", "HP", 25);
stat_set("Potion", "TAGS", "CONSUMABLE;MENU;BATTLE");
stat_set("Potion", "PRICE", 25);
stat_set("Potion", "DESCRIPTION", "Restores 25 HP; usable in battle");

stat_set("Stone Club", "ATK", 5)
stat_set("Stone Club", "PRICE", 20);
stat_set("Stone Club", "TAGS", "EQUIPMENT;WEAPON")

stat_set("Iron Sword", "ATK", 15)
stat_set("Iron Sword", "PRICE", 75);
stat_set("Iron Sword", "ICON", sprite23);
stat_set("Iron Sword", "TAGS", "EQUIPMENT;WEAPON")

stat_set("Battle Axe", "ATK", 20)
stat_set("Battle Axe", "PRICE", 75);
stat_set("Battle Axe", "TAGS", "EQUIPMENT;WEAPON")

stat_set("Great Hammer", "ATK", 25)
stat_set("Great Hammer", "PRICE", 125);
stat_set("Great Hammer", "TAGS", "EQUIPMENT;WEAPON;HEAVY_WEAPON")

stat_set("Great Club", "ATK", 15)
stat_set("Great Club", "PRICE", 25);
stat_set("Great Club", "TAGS", "EQUIPMENT;WEAPON;HEAVY_WEAPON")

stat_set("Leather Armor", "DEF", 10)
stat_set("Leather Armor", "EVADE", 10)
stat_set("Leather Armor", "PRICE", 20);
stat_set("Leather Armor", "TAGS", "EQUIPMENT;ARMOR")

stat_set("Chain Mail", "DEF", 20)
stat_set("Chain Mail", "EVADE", 5)
stat_set("Chain Mail", "PRICE", 200);
stat_set("Chain Mail", "TAGS", "EQUIPMENT;ARMOR")

// PCS
for(var i = 1; i <= 4; ++i)
{
	var pc = "PC"+string(i);
	var base_pc_stat = 10;
	stat_set(pc, "VIT", base_pc_stat)
	stat_set(pc, "ATK", base_pc_stat)
	stat_set(pc, "DEF", base_pc_stat)
	stat_set(pc, "SPD", base_pc_stat)
	stat_set(pc, "MAG", base_pc_stat)
	stat_set(pc, "RES", base_pc_stat)
	stat_set(pc, "EVADE", 0)
	stat_set(pc, "XP", 0)
	stat_set(pc, "TOLEVEL", 10)
	stat_set(pc, "TAGS", "!ATTACK")
	switch(i)
	{
		case 1:
		case 2:
			stat_set(pc, "IN_PARTY", true);
			break;
		default:
			stat_set(pc, "IN_PARTY", false);
			break;	
	}
	
	stat_set(pc, "ICON", sprite_pc1);
	stat_set(pc, "ATTACK_SOUND", sound_swing);
	stat_set(pc, "EQUIP", "");
}

stat_apply_style("PC1", "STYLE_EXPLORER");
stat_apply_style("PC2", "STYLE_AI");
stat_apply_style("PC3", "STYLE_EXPLORER");
stat_apply_style("PC4", "STYLE_EXPLORER");

stat_set("PC2", "ICON", sprite_pc2);
stat_set("PC3", "ICON", sprite_pc3);
stat_set("PC4", "ICON", sprite_pc4);

unique_styles = ds_list_create();
ds_list_add(unique_styles,"");//0
ds_list_add(unique_styles,"");//1
ds_list_add(unique_styles,"STYLE_AI");//2
ds_list_add(unique_styles,"");//3
ds_list_add(unique_styles,"");//4


for (var i = ds_map_find_first(Data.stats); not is_undefined(i) ; i = ds_map_find_next(Data.stats, i))
{
	if( string_copy(i, 1, 5) == "STYLE")
	{
		continue;	
	}
	if (stat_has(i, "VIT"))
	{
		stat_update_hp_max(i);
		stat_set_hp_to_max(i);
		stat_reset_reserved_hp(i);
	}
}