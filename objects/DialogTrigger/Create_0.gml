/// @description Insert description here
// You can write your code in this editor
event_inherited();

persistence_id = data_get_persistence_id(id)
if (once and ever)
{
	if (ds_list_find_index(Data.opened_chests, persistence_id) >= 0)
	{
		instance_destroy();
	}
}