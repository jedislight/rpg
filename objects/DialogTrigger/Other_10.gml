/// @description Play Dialogs
// You can write your code in this editor

var conversation = instance_create_depth(0,0,0,conversation1);
for(var i = 0; i < ds_list_size(conversation.texts); ++i)
{
	var dw = dialog_window_create();
	dw.text = conversation.texts[|i];
	dw.speaker_icon = conversation.speaker_icons[|i];
	dw.event = conversation.events[|i];
}

instance_destroy(conversation);

if (once and ever)
{
	ds_list_add(Data.opened_chests, persistence_id);	
}
if (once)
{
	instance_destroy();	
}