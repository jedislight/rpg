/// @description Insert description here
// You can write your code in this editor
if (instance_find(DialogWindow, 0) != id or instance_number(RoomTransitionText))
{
	exit
}

if (event != noone)
{
	if (event_ran == false)
	{
		event_instance = instance_create_layer(0,0,"Instances_Upper", event);
		show_debug_message("DW: Ran Event: " + object_get_name(event));
		event_ran = true;	
		visible = false;
	}
	else
	{
		if (not instance_exists(event_instance) and text = "")
		{
			instance_destroy();
		}	
	}
}

if (cursor >= string_length(text))
{
	if (input_get_press("OKAY") and not instance_exists(event_instance))
	{
		var action = action_a;
		if (!choice)
		{
			action = action_b;	
		}
		if (script_exists(action))
		{
			script_execute(action)	
		}
		instance_destroy()
	}
}

characters_shown += rate
if (input_get_press("OKAY"))
{
	characters_shown += 1000
}
while(characters_shown >= 1.0 and not dialog_window_is_text_complete())
{
	characters_shown -= 1.0
	shown_text += string_char_at(text, cursor);
	cursor += 1
}

if (dialog_window_is_text_complete() and dialog_window_has_choice())
{
	var old_choice = choice;
	if (input_get_press("LEFT") or input_get_press("RIGHT"))
	{
		choice = !choice;	
	}
	
	if (input_get_press("CANCEL"))
	{
		choice = false;	
	}
	
	if (choice != old_choice)
	{
		audio_play_effect(sound_click);	
	}
}