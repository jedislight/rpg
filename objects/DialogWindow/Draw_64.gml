/// @description Insert description here
// You can write your code in this editor

if (instance_find(DialogWindow, 0) != id or instance_number(RoomTransitionText))
{
	exit
}
var margin = 16;
var left = margin
var right = 512-left
var top = 256 + 32;
var bottom = 512-margin;

if (speaker_icon != noone)
{
	var height = sprite_get_height(speaker_icon);
	var width = sprite_get_width(speaker_icon);
	draw_rectangle_color_analagous_outline(left, top-height, left+width, top, bg_color1, bg_color2, bg_color3, bg_color4)
	draw_sprite_ext(speaker_icon,0, left, top-height,1.0,1.0,0,c_white,1.0);
}
draw_rectangle_color_analagous_outline(left, top, right, bottom, bg_color1, bg_color2, bg_color3, bg_color4)

draw_set_font(font_dialog)
var final_text = shown_text;
if (dialog_window_has_choice() and dialog_window_is_text_complete())
{
	final_text += "\n\n"
	if (choice)
	{
		final_text += " >"+choice_a+"<  " + choice_b;	
	}
	else
	{
		final_text += "  " + choice_a+"  >"+choice_b+"<";
	}
}
draw_text_ext_color(left+margin,top+margin, final_text, font_get_size(draw_get_font())+5, right-left-margin*2, color, color, color, color, 1.0)