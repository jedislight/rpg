/// @description Insert description here
// You can write your code in this editor
collision_this_step = true
if (first_frame)
{
	collision_last_step = true;	
	inside = true;
	exit
}
if (other.moving == false)
{
	if (on_step)
	{
		trigger = true;
		show_debug_message(object_get_name(object_index) + " ON STEP")
	}
	
	if (inside == false)
	{
		inside = true
		if (on_enter)
		{
			trigger = true	
			show_debug_message(object_get_name(object_index) + " ON ENTER")
		}
	}	
}