/// @description Insert description here
// You can write your code in this editor
event_inherited();

var description = "";
if(stat_has(item, "DESCRIPTION"))
{
	description = stat_get(item, "DESCRIPTION");	
}

var name = item;

var price = stat_get(item, "PRICE");

text = "Purchase " + name + " for " + string(price) + "?"

text += "\n"+description;

text += "\n";

persistence_id = data_get_persistence_id(id)

if (ds_list_find_index(Data.opened_chests, persistence_id) >= 0)
{
	instance_destroy();
}

