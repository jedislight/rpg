function stat_has_tag(argument0, argument1) {
	var stats = Data.stats[? argument0];
	var result = false;
	if (is_undefined(stats))
	{
		return false;	
	}
	if (ds_map_exists(stats, "TAGS"))
	{
		var tags = ds_list_from_semicolon_delimited_string(stats[? "TAGS"])
		result = ds_list_find_index(tags, argument1) >= 0;
		ds_list_destroy(tags);	
	}

	if(not result and stat_has(argument0, "STYLE"))
	{
		var style = stat_get(argument0, "STYLE");
		tags = ds_list_from_semicolon_delimited_string(stat_get(style, "TAGS"));
		result = ds_list_find_index(tags, argument1) >= 0;
		ds_list_destroy(tags);
	}

	return result;


}
