function input_get_press(argument0) {
	switch(argument0)
	{
		case "UP": return keyboard_check_pressed(vk_up);
		case "DOWN": return keyboard_check_pressed(vk_down);
		case "LEFT": return keyboard_check_pressed(vk_left);
		case "RIGHT": return keyboard_check_pressed(vk_right);
		case "OKAY": return keyboard_check_pressed(vk_enter) or keyboard_check_pressed(vk_space);
		case "CANCEL": return keyboard_check_pressed(vk_shift) or keyboard_check_pressed(vk_escape);
		default: show_debug_message("INVALID INPUT:" + string(argument0)); return false;
	}


}
