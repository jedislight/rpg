function number_of_characters_in_party() {
	var count = 0;
	for (var i = 1; i <= 4; i++)
	{
		var pc = "PC" + string(i);
		if(stat_get(pc, "IN_PARTY"))
		{
			count += 1;	
		}
	}

	return count;


}
