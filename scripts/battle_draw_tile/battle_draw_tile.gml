function battle_draw_tile(argument0, argument1, argument2, argument3, argument4, argument5, argument6) {
	draw_rectangle_color_analagous_outline(argument0, argument1, argument0 + argument2, argument1 + argument3, c_dkgray, argument4, c_dkgray, c_dkgray)

	var icon = stat_get(argument5, "ICON");
	var icon_frames = sprite_get_number(icon);
	if (battle_is_alive(argument5))
	{
		draw_sprite(icon, (current_time/250) mod icon_frames, argument0, argument1);
	}

	if (manual_target == argument5)
	{
		for(var i = 0; i < 7; ++i)
		{
			var offset = power((sin(current_time/100) + 1), 2);
			offset *= 5;
			draw_rectangle_color(argument0+offset, argument1+offset, argument0 + argument2 - offset, argument1 + argument3 - offset, c_white, c_white, c_white, c_white, true)		
		}
	}

	draw_text_outlined(argument0,argument1, argument5, c_white);
	if (argument6 != "")
	{
		draw_text_outlined(argument0, argument1+argument3-string_height(argument6), argument6, c_white);
	}


}
