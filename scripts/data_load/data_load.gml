function data_load(argument0) {
	if(not directory_exists("saves"))
	{
		directory_create("saves")
	}

	if (not file_exists("saves/"+string(argument0)))
	{
		show_debug_message("Could not load save " + string(argument0) + ", file not found");
		return;	
	}

	if (instance_number(Data) == 0)
	{
		instance_create_depth(0,0,0, Data)	
	}

	ini_open("saves/"+string(argument0))

	var xx = ini_read_real("Location", "X", 0)
	var yy = ini_read_real("Location", "Y", 0)
	var map_name = ini_read_string("Location", "Map", "DebugIsland")

	if (not instance_number(PartyPawn))
	{
		instance_create_layer(xx,yy, "Instances", PartyPawn);
	}

	PartyPawn.x = xx;
	PartyPawn.y = yy;
	PartyPawn.target_x = x
	PartyPawn.target_y = y
	PartyPawn.loading_position = true;

	Data.gold = ini_read_real("Inventory", "Gold", Data.gold);
	Data.random_encounter_accumulator = ini_read_real("Misc", "Encounter_Steps", Data.random_encounter_accumulator)

	ds_list_read(Data.opened_chests, ini_read_string("Misc", "Chest_State", ds_list_write(Data.opened_chests)));
	ds_list_read(Data.known_styles, ini_read_string("Misc", "Known_Styles", ds_list_write(Data.known_styles)));

	ds_map_read(Data.key_items, ini_read_string("Inventroy", "Items", ds_map_write(Data.key_items)));

	for (var i = 1; i <= 4; ++i)
	{
		ds_map_read(Data.stats[? "PC"+string(i)], ini_read_string("Characters", "PC"+string(i), ds_map_write(Data.stats[? "PC" + string(i)])));	
	}

	PartyPawn.persistent = true;
	room_goto(asset_get_index(map_name))

	show_debug_message("Loaded save " + string(argument0));

	ini_close()


}
