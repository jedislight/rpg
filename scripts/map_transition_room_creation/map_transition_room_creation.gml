function map_transition_room_creation() {
	show_debug_message("Map transition from " + room_get_name(global.previous_room) + " to " + room_get_name(room))
	if (global.previous_room != noone)
	{
		var xx = 0
		var yy = 0
		var n = instance_find(MapTransition, 0)
		for (var i = 0; i < instance_number(MapTransition); ++i)
		{
			var c = instance_find(MapTransition, i);
			if (c.transition_to == global.previous_room)
			{
				n = c;
			}
		}
		if (instance_exists(n))
		{
			xx = n.x
			yy = n.y
		}
		if (instance_number(PartyPawn) == 0)
		{
			instance_create_layer(xx,yy, "Instances_Upper", PartyPawn)	
		}
		else
		{
			var n = instance_find(PartyPawn, 0);
			if (n.loading_position)
			{
				n.loading_position = false;
				show_debug_message("using loaded position")
			}
			else
			{
				show_debug_message("using map transition position")
				n.x = xx;
				n.y = yy;
				n.target_x = n.x
				n.target_y = n.y
			}
		}
	}

	var rtt = instance_create_layer(0,0,"Instances_Upper", RoomTransitionText);
	rtt.text = room_get_name(room);

	global.map_music = music_tropicalfantasy


}
