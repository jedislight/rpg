function data_save(argument0) {
	if(not directory_exists("saves"))
	{
		directory_create("saves")
	}

	ini_open("saves/"+string(argument0))

	ini_write_real("Location", "X", PartyPawn.x)
	ini_write_real("Location", "Y", PartyPawn.y)
	ini_write_string("Location", "Map", room_get_name(room))

	ini_write_real("Inventory", "Gold", Data.gold);
	ini_write_string("Inventroy", "Items", ds_map_write(Data.key_items));

	ini_write_real("Misc", "Encounter_Steps", Data.random_encounter_accumulator)

	ini_write_string("Misc", "Chest_State", ds_list_write(Data.opened_chests));

	ini_write_string("Misc", "Known_Styles", ds_list_write(Data.known_styles));

	for (var i = 1; i <= 4; ++i)
	{
		ini_write_string("Characters", "PC"+string(i), ds_map_write(Data.stats[? "PC" + string(i)]));	
	}

	show_debug_message("Saved in slot " + string(argument0))

	ini_close()


}
