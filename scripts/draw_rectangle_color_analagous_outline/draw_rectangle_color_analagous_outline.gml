function draw_rectangle_color_analagous_outline(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7) {
	draw_rectangle_color(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, false)
	draw_rectangle_color(
		argument0,
		argument1,
		argument2,
		argument3,
		color_analagous(argument4),
		color_analagous(argument5),
		color_analagous(argument6),
		color_analagous(argument7),
		true
	)


}
