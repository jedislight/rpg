function stat_is_equipped(argument0, argument1) {
	var equipment = ds_list_from_semicolon_delimited_string(stat_get(argument0, "EQUIP"));
	var result = ds_list_find_index(equipment, argument1) >= 0;
	ds_list_destroy(equipment);
	return result;


}
