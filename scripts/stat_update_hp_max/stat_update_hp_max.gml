function stat_update_hp_max(argument0) {
	stat_set(argument0, "HP_MAX", stat_get_effect(argument0, "VIT"))
	if (not stat_has(argument0, "HP"))
	{
		stat_set(argument0, "HP", 1);	
	}
	stat_set(argument0, "HP", min(stat_get(argument0, "HP"), stat_get(argument0, "HP_MAX")))


}
