function battle_is_alive(argument0) {
	if(stat_has(argument0, "IN_PARTY") and not stat_get(argument0, "IN_PARTY"))
	{
		return false;	
	}
	return stat_get(argument0, "HP") > 0;


}
