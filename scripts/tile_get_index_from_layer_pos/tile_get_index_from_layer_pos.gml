function tile_get_index_from_layer_pos(argument0, argument1, argument2) {
	var lay_id = layer_get_id(argument0);
	var map_id = layer_tilemap_get_id(lay_id);
	var data = tilemap_get_at_pixel(map_id, argument1, argument2);
	if (data == -1)
	{
		return -1;	
	}
	var tile_index = tile_get_index(data);
	return tile_index;


}
