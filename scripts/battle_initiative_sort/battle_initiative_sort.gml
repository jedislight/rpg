function battle_initiative_sort() {
	repeat(ds_list_size(Battle.initiative) *2)
	{
		for (var i = 1; i < ds_list_size(Battle.initiative); ++i)
		{
			var a = Battle.initiative[| i-1];
			var b = Battle.initiative[| i];
			var a_roll = stat_get_effect_roll(a, "SPD")
			var b_roll = stat_get_effect_roll(b, "SPD")
	
			if ( a_roll < b_roll)
			{
				Battle.initiative[| i-1] = b;
				Battle.initiative[|i] = a;
			}
		}
	}


}
