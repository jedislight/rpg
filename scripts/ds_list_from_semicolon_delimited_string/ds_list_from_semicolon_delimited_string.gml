function ds_list_from_semicolon_delimited_string(argument0) {
	var result = ds_list_create();
	var current = ""
	for (var i = 1; i <= string_length(argument0); ++i)
	{
		var char = string_char_at(argument0, i);
		if (char == ";")
		{
			ds_list_add(result, current);
			current = "";
		}
		else
		{
			current += char;	
		}
	}

	ds_list_add(result, current);

	return result;


}
