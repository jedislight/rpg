function color_analagous(argument0) {
	return make_color_hsv(
		(color_get_hue(argument0)+21) mod 255,
		color_get_saturation(argument0),
		color_get_value(argument0)
	)


}
