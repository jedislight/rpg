function ds_list_dedupe(argument0) {
	for(var i = ds_list_size(argument0)-1; i >= 0; --i)
	{
		var value = argument0[|i];
		if(ds_list_find_index(argument0, value) != i)
		{
			ds_list_delete(argument0, i);
		}
	}


}
