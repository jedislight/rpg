function color_invert_value(argument0) {
	return make_color_hsv(color_get_hue(argument0), color_get_saturation(argument0), 255-color_get_value(argument0))


}
