function stat_can_equip(argument0, argument1) {
	var style = stat_get(argument0, "STYLE");
	var style_tags = ds_list_from_semicolon_delimited_string(stat_get(style, "TAGS"));
	var character_tags = ds_list_from_semicolon_delimited_string(stat_get(argument0, "TAGS"));
	var equipment_tags = ds_list_from_semicolon_delimited_string(stat_get(argument1, "TAGS"));
	var all_tags = ds_list_create();
	ds_list_copy(all_tags, character_tags);
	for(var i = 0; i < ds_list_size(style_tags); ++i)
	{
		ds_list_add(all_tags, style_tags[|i]);	
	}
	ds_list_destroy(style_tags);
	ds_list_destroy(character_tags);
	var result = ds_list_subset(equipment_tags, all_tags);
	ds_list_destroy(equipment_tags);
	ds_list_destroy(all_tags);
	return result;


}
