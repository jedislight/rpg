function stat_apply_style(argument0, argument1) {
	if (stat_has(argument0, "STYLE"))
	{
		var style = stat_get(argument0, "STYLE");
		stat_adjust(argument0, "ATK", -power(stat_get(style, "ATK"), 2))
		stat_adjust(argument0, "DEF", -power(stat_get(style, "DEF"), 2))
		stat_adjust(argument0, "MAG", -power(stat_get(style, "MAG"), 2))
		stat_adjust(argument0, "RES", -power(stat_get(style, "RES"), 2))
		stat_adjust(argument0, "SPD", -power(stat_get(style, "SPD"), 2))
		stat_adjust(argument0, "VIT", -power(stat_get(style, "VIT"), 2))
		stat_adjust(argument0, "EVADE", -stat_get(style, "EVADE"))
	}
	stat_set(argument0, "STYLE", argument1);

	stat_adjust(argument0, "ATK", power(stat_get(argument1, "ATK"), 2))
	stat_adjust(argument0, "DEF", power(stat_get(argument1, "DEF"), 2))
	stat_adjust(argument0, "MAG", power(stat_get(argument1, "MAG"), 2))
	stat_adjust(argument0, "RES", power(stat_get(argument1, "RES"), 2))
	stat_adjust(argument0, "SPD", power(stat_get(argument1, "SPD"), 2))
	stat_adjust(argument0, "VIT", power(stat_get(argument1, "VIT"), 2))
	stat_adjust(argument0, "EVADE", stat_get(argument1, "EVADE"))

	stat_update_hp_max(argument0);

	// add


}
