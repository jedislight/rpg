function battle_manual_target_step() {
	var party_target = (instance_number(BattleActionMenu) > 0 and BattleActionMenu.manual_targeting_party);
	var random_target = (instance_number(BattleActionMenu) > 0 and BattleActionMenu.is_enemy);
	if (party_target)
	{
		if (Battle.manual_target == "" or not battle_is_alive(Battle.manual_target))
		{
			Battle.manual_target = battle_get_first_party_member();
		}
	
		var party_index = real(string_char_at(Battle.manual_target, 3));
		var original_value = party_index;
	
		var wrap_tries = 5;
		do
		{
			wrap_tries -= 1;
			if (wrap_tries == 0)
			{
				return;	
			}
			if(random_target)
			{
				party_index = irandom_range(1,4);
			}
			else if (input_get_press("DOWN"))
			{
				party_index += 1;	
			}
			else if (input_get_press("UP"))
			{
				party_index -= 1;	
			}
			if( party_index > 4)
			{
				party_index = 1;	
			}
			if (party_index < 1)
			{
				party_index = 4;	
			}
		} until (stat_exists("PC"+string(party_index)+"Copy") and battle_is_alive("PC"+string(party_index)+"Copy"));
	
		if (original_value != party_index)
		{
			audio_play_effect(sound_interface1);
			Battle.manual_target = "PC"+string(party_index)+"Copy";
			show_debug_message("Manual Target:" + string(Battle.manual_target));
		}
	}
	else
	{
		if (Battle.manual_target == "" or not battle_is_alive(Battle.manual_target))
		{
			Battle.manual_target = battle_get_first_enemy();
		}

		var enemy_index = ds_list_find_index(Battle.enemies_copies, Battle.manual_target);

		var original_value = enemy_index;
		var wrap_tries = 10;
		do
		{
			wrap_tries -= 1;
			if( wrap_tries <= 0)
			{
				break;	
			}
			if (random_target)
			{
				enemy_index = irandom_range(0,9);	
			}
			else if (input_get_press("DOWN"))
			{
				enemy_index = enemy_index + 1;
			}

			else if (input_get_press("UP"))
			{
				enemy_index = enemy_index - 1;
			}

			else if (input_get_press("LEFT"))
			{
				enemy_index = enemy_index - 3;
			}

			else if (input_get_press("RIGHT"))
			{
				enemy_index = enemy_index + 3;
			}
	
			while (enemy_index < 0)
			{
				enemy_index += 9;	
			}
			while (enemy_index >= 9)
			{
				enemy_index -= 9;	
			}
		}until(enemy_index < ds_list_size(Battle.enemies_copies) and Battle.enemies_copies[| enemy_index] != "" and battle_is_alive(Battle.enemies_copies[| enemy_index]));

		Battle.manual_target = Battle.enemies_copies[| enemy_index];

		if (original_value != enemy_index)
		{
			audio_play_effect(sound_interface1);	
			show_debug_message("Manual Target:" + string(Battle.manual_target));
		}
	}

	if (is_undefined(Battle.manual_target))
	{
		Battle.manual_target = "";	
	}


}
