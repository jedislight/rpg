function party_restore_hp() {
	for (var i = ds_map_find_first(Data.stats); not is_undefined(i) ; i = ds_map_find_next(Data.stats, i))
	{
		if (not battle_is_pc(i))
		{
			continue;
		}
	
		stat_set_hp_to_max(i);
	}


}
