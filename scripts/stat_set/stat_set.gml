function stat_set(argument0, argument1, argument2) {
	if( not ds_map_exists(Data.stats, argument0))
	{
		Data.stats[? argument0]  = ds_map_create();	
	}

	ds_map_replace(Data.stats[? argument0], argument1, argument2)
	show_debug_message("Set " + argument0 + "'s " + argument1 + " to " + string(argument2))


}
