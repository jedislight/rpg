function stat_adjust(argument0, argument1, argument2) {
	var max_stat_name = argument1 + "_MAX";
	var reserved_stat_name = "RESERVED_"  + argument1;
	var max_stat = undefined;
	var reserved_stat = 0;
	if (stat_has(argument0, max_stat_name))
	{
		max_stat = stat_get(argument0, max_stat_name);
	}
	if (stat_has(argument0, reserved_stat_name))
	{
		reserved_stat = stat_get(argument0, reserved_stat_name);	
	}

	if (is_undefined(max_stat))
	{
		stat_set(argument0, argument1, max(0,stat_get(argument0, argument1) + argument2));
	}
	else
	{
		stat_set(argument0, argument1, min(max_stat-reserved_stat, max(0,stat_get(argument0, argument1) + argument2)));
	}



}
