function battle_enemies_defeated() {
	for(var i = 0; i < ds_list_size(Battle.initiative); ++i)
	{
		var combatant = Battle.initiative[|i];
		if (not battle_is_pc(combatant))
		{
			if (battle_is_alive(combatant))
			{
				return false;	
			}
		}
	}

	return true;


}
