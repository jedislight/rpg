function stat_find_next_style_tag_to_learn(argument0, argument1) {
	var style_tags = ds_list_from_semicolon_delimited_string(stat_get(argument1, "TAGS"));
	var character_tags = ds_list_from_semicolon_delimited_string(stat_get(argument0, "TAGS"));
	var to_learn = "";
	for (var i = 0; i < ds_list_size(style_tags); ++i)
	{
		if (to_learn != "")
		{
			break;	
		}
		var full_tag = style_tags[|i];
		var tag = string_delete(full_tag, 1, 1); 
		switch(string_char_at(full_tag, 1))
		{
			case "+":
				if (ds_list_find_index(character_tags, tag) < 0)
				{
					to_learn = full_tag;
				}
				break;
		}
	}

	ds_list_destroy(style_tags);
	ds_list_destroy(character_tags);

	return to_learn;


}
