function stat_get_tags_with_style(argument0) {
	var tags_string = stat_get(argument0,"TAGS");
	if (stat_has(argument0, "STYLE"))
	{
		tags_string += ";" + stat_get(stat_get(argument0, "STYLE"), "TAGS");	
	}

	return tags_string


}
