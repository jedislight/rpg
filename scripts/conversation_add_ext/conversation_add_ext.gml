function conversation_add_ext(argument0, argument1, argument2) {
	ds_list_add(texts, argument1);

	var icon = noone;
	if (is_real(argument0) and object_exists(argument0))
	{
		icon = object_get_sprite(argument0);
	}
	else if (is_string(argument0) and stat_exists(argument0) and stat_has(argument0, "ICON"))
	{
		icon = stat_get(argument0, "ICON");	
	}
	else if(is_real(argument0) and instance_exists(argument0))
	{
		icon = argument0.sprite_index;
	}
	ds_list_add(speaker_icons, icon);

	ds_list_add(events, argument2);


}
