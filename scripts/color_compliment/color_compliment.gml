function color_compliment(argument0) {
	return make_color_hsv(255-color_get_hue(argument0), color_get_saturation(argument0), color_get_value(argument0))


}
