function battle_heal(argument0, argument1) {
	var heal_effect = stat_get_fudged_effect(argument0, "MAG")
	var defense_divisor = stat_get_divisor(argument0, "MAG")
	var damage = round(heal_effect / defense_divisor);

	show_debug_message(argument0 + " healing " + argument1 + " for " + string(damage))
	stat_adjust(argument1, "HP", damage)
	text_flyout_create(argument1, string(damage), c_lime);
	audio_play_effect(sound_spell)



}
