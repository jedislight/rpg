function key_item_check(argument0) {
	var value = ds_map_find_value(Data.key_items, argument0);
	if (is_undefined(value))
	{
		return false;	
	}
	else
	{
		return value > 0;
	}


}
