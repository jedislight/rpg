function item_apply(argument0, argument1) {
	var item_name = argument1;
	var owner = argument0;
	for (var i = ds_map_find_first(Data.stats[? item_name]); not is_undefined(i); i = ds_map_find_next(Data.stats[? item_name], i))
	{
		if (stat_has(owner, i) and i!="TAGS")
		{
			stat_adjust(owner, i, stat_get(item_name, i));
		}
	}
	key_item_remove_one(item_name);



}
