function party_equipped_count(argument0) {
	var count = 0;
	for (var i = 1; i <= number_of_characters_in_party(); ++i)
	{
		var pc = "PC" + string(i);
		var equipment = ds_list_from_semicolon_delimited_string(stat_get(pc, "EQUIP"));
		if(ds_list_find_index(equipment, argument0))
		{
			++count;	
		}
		ds_list_destroy(equipment)
	}

	return count;


}
