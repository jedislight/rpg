function ds_list_subset(argument0, argument1) {
	for(var i = 0; i < ds_list_size(argument0); ++i)
	{
		if (ds_list_find_index(argument1, argument0[|i]) < 0)
		{
			return false;	
		}
	}

	return true;


}
