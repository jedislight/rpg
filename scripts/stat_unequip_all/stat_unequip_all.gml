function stat_unequip_all(argument0) {
	if (stat_has(argument0, "EQUIP"))
	{
		var equipment = ds_list_from_semicolon_delimited_string(stat_get(argument0, "EQUIP"));

		for(var i = 0; i < ds_list_size(equipment); ++i)
		{
			var item = equipment[|i];
			if (item == "")
			{
				continue;	
			}
	
			stat_remove_equip(argument0, item);
		}

		stat_set(argument0, "EQUIP", "");

		ds_list_destroy(equipment);
	}


}
