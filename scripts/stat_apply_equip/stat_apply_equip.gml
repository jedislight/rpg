function stat_apply_equip(argument0, argument1) {
	if(stat_has(argument1, "ATK"))
		stat_adjust(argument0, "ATK", stat_get(argument1, "ATK"))

	if(stat_has(argument1, "DEF"))
		stat_adjust(argument0, "DEF", stat_get(argument1, "DEF"))
	
	if(stat_has(argument1, "SPD"))
		stat_adjust(argument0, "SPD", stat_get(argument1, "SPD"))

	if(stat_has(argument1, "VIT"))
		stat_adjust(argument0, "VIT", stat_get(argument1, "VIT"))
	
	if(stat_has(argument1, "EVADE"))
		stat_adjust(argument0, "EVADE", stat_get(argument1, "EVADE"))


}
