function store_item_purchase() {
	var price = stat_get(item, "PRICE");
	if (Data.gold >= price)
	{
		if (item_count(item) < 9)
		{
			Data.gold -= price;
			item_add_one(item);
			if(limited)
			{
				ds_list_add(Data.opened_chests, persistence_id);
			}
		}
		else
		{
			var dw = dialog_window_create();
			dw.text = "Cannot carry anymore "+item+"!"
			audio_play_effect(sound_negative);		
		}
	}
	else
	{
		var dw = dialog_window_create();
		dw.text = "Not enough gold!"
		audio_play_effect(sound_negative);
	}


}
