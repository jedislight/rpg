function battle_combatant_turn(argument0) {
	if (instance_number(BattleActionMenu) > 0 and BattleActionMenu.owner != argument0)
	{
		// old menu - clean it up
		with(BattleActionMenu){instance_destroy()}
		return false;
	}
	if (not instance_number(BattleActionMenu))
	{
		battle_create_action_menu(argument0);
	}
	if (BattleActionMenu.done)
	{
		Battle.manual_target = ""
		with(BattleActionMenu){instance_destroy()}
		return true;
	}
	else
	{
		return false;
	}



}
