function music_switch_track(argument0) {
	if(global.playing_music == audio_get_name(argument0))
	{
		return;	
	}

	while(ds_stack_size(global.music_stack))
	{
		audio_stop_sound(ds_stack_pop(global.music_stack));
	}

	audio_stop_sound(asset_get_index(global.playing_music));
	ds_stack_push(global.music_stack, audio_play_sound(argument0, 1, true));
	global.playing_music = audio_get_name(argument0);

	show_debug_message("Music: " + audio_get_name(argument0));


}
