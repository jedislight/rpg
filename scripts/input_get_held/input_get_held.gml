function input_get_held(argument0) {
	switch(argument0)
	{
		case "UP": return keyboard_check_direct(vk_up);
		case "DOWN": return keyboard_check_direct(vk_down);
		case "LEFT": return keyboard_check_direct(vk_left);
		case "RIGHT": return keyboard_check_direct(vk_right);
		case "OKAY": return keyboard_check_direct(vk_enter) or keyboard_check_direct(vk_space);
		case "CANCEL": return keyboard_check_direct(vk_shift) or keyboard_check_direct(vk_escape);
		default: show_debug_message("INVALID INPUT:" + string(argument0)); return false;
	}


}
