function battle_get_first_enemy() {
	var enemy = "";
	for (var i = 0; i < 9; ++i)
	{
		enemy = ""
		if (ds_list_size(Battle.enemies_copies) > i)
		{
			enemy = Battle.enemies_copies[|i];	
		}
		if (enemy == "" or not battle_is_alive(enemy))
		{
			enemy = "";
			continue;	
		}
	
		break;
	}

	return enemy;


}
