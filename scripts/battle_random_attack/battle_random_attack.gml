function battle_random_attack(argument0) {
	var available_targets = ds_list_create()
	var is_pc = battle_is_pc(argument0);
	for( var i = 0; i < ds_list_size(Battle.initiative); ++i)
	{
		var target = Battle.initiative[|i];
		if( battle_is_pc(target) != is_pc)
		{
			if (battle_is_alive(target))
			{
				ds_list_add(available_targets, target);
			}
		}
	}

	ds_list_shuffle(available_targets);
	if (ds_list_size(available_targets) != 0)
	{
		battle_attack(argument0, available_targets[|0])	
	}

	ds_list_destroy(available_targets)


}
