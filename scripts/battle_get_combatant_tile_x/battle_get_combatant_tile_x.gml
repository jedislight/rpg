function battle_get_combatant_tile_x(argument0) {
	if (battle_is_pc(argument0))
	{
		var pc_index = real(string_char_at(argument0, 3));
		return battle_get_pc_tile_x(pc_index);
	}
	else
	{
		var enemy_index = ds_list_find_index(Battle.enemies_copies, argument0);
		return battle_get_enemy_tile_x(enemy_index);
	}


}
