function battle_try_flee(argument0) {
	var enemy_roll = 0;
	var pc_roll = stat_get_effect_roll(argument0, "SPD");
	for(var i = 0; i < ds_list_size(Battle.initiative); ++i)
	{
		var combatant = Battle.initiative[|i];
		if (not battle_is_pc(combatant))
		{
			if (battle_is_alive(combatant))
			{
				enemy_roll = max(enemy_roll, stat_get_effect_roll(combatant, "SPD"))
			}
		}
	}

	show_debug_message("Flee attempt: " + string(pc_roll) + " vs " + string(enemy_roll))
	var success = enemy_roll < pc_roll;

	if (success)
	{
		battle_flee();
		return true;
	}
	else
	{
		audio_play_effect(sound_negative_2)	;
		return false;
	}


}
