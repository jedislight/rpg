function battle_get_first_party_member() {
	for(var i = 1; i <= number_of_characters_in_party(); ++i)
	{
		var pc = "PC" + string(i) + "Copy";
		if(battle_is_alive(pc))
		{
			return pc;
		}
	}

	return ""


}
