function stat_level_up(argument0) {
	var style_name = stat_get(argument0, "STYLE")
	var style = Data.stats[? style_name];

	var dw = noone;
	if (layer_exists("Instances_Uppper"))
	{
		var dw = instance_create_layer(0,0,"Instances_Upper", DialogWindow)
		dw.text = argument0 + " has leveled up!"
	}
	for (var i = ds_map_find_first(style); not is_undefined(i); i = ds_map_find_next(style, i))
	{
		if (i == "TAGS" or i == "EVADE")
		{
			continue;	
		}
		var amount = style[? i];
		stat_adjust(argument0, i, amount);
		if(instance_exists(dw))
			dw.text += "\n" + i + " + " + string(amount);
	}

	if (stat_has(argument0, "TOLEVEL"))
	{
		stat_adjust(argument0, "XP", -stat_get(argument0, "TOLEVEL"));
		stat_set(argument0, "TOLEVEL", round(stat_get(argument0, "TOLEVEL") + 10));
	}
	stat_update_hp_max(argument0);
	if(instance_exists(dw))
		dw.text += "\n HP = " + string(stat_get(argument0, "HP_MAX"));

	var style_tag_to_learn = stat_find_next_style_tag_to_learn(argument0, style_name);
	if(style_tag_to_learn != "")
	{
		var tag = string_delete(style_tag_to_learn, 1, 1);
		var learning_type = string_char_at(style_tag_to_learn, 1);
		if (learning_type == "+")
		{
			var tags = ds_list_from_semicolon_delimited_string(stat_get(argument0, "TAGS"));
			ds_list_add(tags, tag);
			ds_list_remove_by_value(tags, "");
			stat_set(argument0, "TAGS", ds_list_join(tags, ";"));
			ds_list_destroy(tags);
			if(instance_exists(dw))	
				dw.text += "\n\nLearned " + tag + " permenantly!";
		}
		else
		{
			show_debug_message("UNKNOWN LEARNING TYPE ON TAG:" + style_tag_to_learn);
		}
	
		var next_tag_to_learn = stat_find_next_style_tag_to_learn(argument0, style_name);
		if (next_tag_to_learn == "")
		{
			if(instance_exists(dw))
				dw.text += "\n\n"+argument0+ " has mastered " + style_name + "!"	
		}
	}


}
