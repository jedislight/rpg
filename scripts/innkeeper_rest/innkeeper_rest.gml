function innkeeper_rest() {
	with(InnkeeperNPC)
	{
		if (Data.gold >= price)
		{
			Data.gold -= price;
		
			for (var i = ds_map_find_first(Data.stats); not is_undefined(i) ; i = ds_map_find_next(Data.stats, i))
			{
				if (not battle_is_pc(i))
				{
					continue;
				}
	
				stat_reset_reserved_hp(i);
			}
		
			party_restore_hp()
			audio_play_effect(sound_load);
			data_save("auto");
		}
		else
		{
			var dw = dialog_window_create()
			dw.text = "Sorry, you don't have enough."
		}
	}


}
