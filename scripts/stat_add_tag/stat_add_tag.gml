function stat_add_tag(argument0, argument1) {
	var tags = ds_list_from_semicolon_delimited_string(stat_get(argument0, "TAGS"));
	ds_list_add(tags, argument1);
	ds_list_remove_by_value(tags, "");
	stat_set(argument0, "TAGS", ds_list_join(tags, ";"));
	ds_list_destroy(tags);


}
