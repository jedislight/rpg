function music_pop_track() {
	audio_stop_sound(ds_stack_pop(global.music_stack));
	if (ds_stack_size(global.music_stack))
	{
		audio_resume_sound(ds_stack_top(global.music_stack));
	}
	global.playing_music = audio_get_name(ds_stack_top(global.music_stack));

	show_debug_message("Music Pop");


}
