function input_lock_check() {
	if (argument_count > 0)
	{
		return global.input_locks[? argument[0]]	
	}
	else
	{
		for (var it = ds_map_find_first(global.input_locks); not is_undefined(it); it = ds_map_find_next(global.input_locks, it))
		{
			if (global.input_locks[? it])
			{
				return true	
			}
		}
		return false
	}


}
