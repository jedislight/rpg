function stat_get_actions(argument0) {
	var actions = ds_list_create();

	var tags_string = stat_get_tags_with_style(argument0);
	var tags = ds_list_from_semicolon_delimited_string(tags_string);
	for (var i =0 ; i < ds_list_size(tags); ++i)
	{
		var tag = tags[|i];
		if(string_char_at(tag, 1) == "!")
		{
			ds_list_add(actions, tag);
		}
	}
	ds_list_destroy(tags);
	ds_list_sort(actions, true);
	ds_list_dedupe(actions);
	return actions;


}
