function text_flyout_create(argument0, argument1, argument2) {
	var xx = battle_get_combatant_tile_x(argument0);
	var yy = battle_get_combatant_tile_y(argument0);

	var tfo = instance_create_layer(xx, yy, "Instances_Upper", TextFlyout);
	tfo.text = argument1;
	tfo.color = argument2;


}
