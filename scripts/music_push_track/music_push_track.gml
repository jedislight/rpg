function music_push_track(argument0) {
	if(global.playing_music == audio_get_name(argument0))
	{
		return;	
	}

	audio_pause_sound(ds_stack_top(global.music_stack));
	ds_stack_push(global.music_stack, audio_play_sound(argument0, 1, true));
	global.playing_music = audio_get_name(argument0);

	show_debug_message("Music Push: " + audio_get_name(argument0));


}
