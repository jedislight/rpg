function battle_attack(argument0, argument1) {
	var attack_effect = stat_get_fudged_effect(argument0, "ATK")
	var defense_divisor = stat_get_divisor(argument1, "DEF")
	var damage = round(attack_effect / defense_divisor);
	var hit_chance = 100 - stat_get(argument1, "EVADE");
	var is_hit = irandom(100) < hit_chance;
	if (is_hit)
	{
		show_debug_message(argument0 + " attacking " + argument1 + " for " + string(damage))
		stat_adjust(argument1, "HP", -damage)
		text_flyout_create(argument1, string(damage), c_white);
		audio_play_effect(stat_get(argument0, "ATTACK_SOUND"))
	}
	else
	{
		show_debug_message(argument0 + " misses " + argument1)
		text_flyout_create(argument1, "MISS", c_white);
		audio_play_effect(sound_swing3)
	}



}
