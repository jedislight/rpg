function stat_equip(argument0, argument1) {
	var equipment = ds_list_from_semicolon_delimited_string(stat_get(argument0, "EQUIP"));
	var new_tags = ds_list_from_semicolon_delimited_string(stat_get(argument1, "TAGS"));
	ds_list_remove_by_value(new_tags, "EQUIPMENT");
	var prev_index = ds_list_find_index(equipment, argument1);
	if (prev_index < 0)
	{
		for (var i = 0; i < ds_list_size(equipment); ++i)
		{
			var equipped = equipment[|i];
			if (equipped == "")
			{
				continue;	
			}
	
			var equipped_tags = ds_list_from_semicolon_delimited_string(stat_get(equipped, "TAGS"));
			ds_list_remove_by_value(equipped_tags, "EQUIPMENT");
			var unequip_this = ds_list_match_any(equipped_tags, new_tags);
			ds_list_destroy(equipped_tags);
	
			if (unequip_this)
			{
				stat_remove_equip(argument0, equipped);	
				ds_list_delete(equipment, i);
				--i;
			}
		}

		stat_apply_equip(argument0, argument1);
		ds_list_add(equipment, argument1);
	}
	else
	{
		stat_remove_equip(argument0, argument1);
		ds_list_delete(equipment, prev_index);
	}

	var blank_index = ds_list_find_index(equipment, "");
	while(blank_index >= 0)
	{
		ds_list_delete(equipment, blank_index);
		blank_index = ds_list_find_index(equipment, "");	
	}

	stat_set(argument0, "EQUIP", ds_list_join(equipment, ";"));
	ds_list_destroy(new_tags);
	ds_list_destroy(equipment);


}
