function stat_reserve_hp(argument0, argument1) {
	var amount = argument1;
	if (amount < 1)
	{
		amount = ceil(stat_get(argument0, "HP_MAX") * argument1);
	}

	stat_adjust(argument0, "RESERVED_HP", amount);
	stat_set(argument0, "HP", clamp(stat_get(argument0, "HP"),1,stat_get(argument0, "HP_MAX") - stat_get(argument0, "RESERVED_HP"))); 


}
