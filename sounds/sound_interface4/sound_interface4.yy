{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sound_interface4",
  "duration": 0.0,
  "parent": {
    "name": "interface",
    "path": "folders/Sounds/Effects/RPG Sound Pack/interface.yy",
  },
  "resourceVersion": "1.0",
  "name": "sound_interface4",
  "tags": [],
  "resourceType": "GMSound",
}