{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sound_door",
  "duration": 0.0,
  "parent": {
    "name": "world",
    "path": "folders/Sounds/Effects/RPG Sound Pack/world.yy",
  },
  "resourceVersion": "1.0",
  "name": "sound_door",
  "tags": [],
  "resourceType": "GMSound",
}