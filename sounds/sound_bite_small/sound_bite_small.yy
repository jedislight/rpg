{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sound_bite_small",
  "duration": 0.0,
  "parent": {
    "name": "beetle",
    "path": "folders/Sounds/Effects/RPG Sound Pack/NPC/beetle.yy",
  },
  "resourceVersion": "1.0",
  "name": "sound_bite_small",
  "tags": [],
  "resourceType": "GMSound",
}