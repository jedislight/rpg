{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sound_giant4",
  "duration": 0.0,
  "parent": {
    "name": "giant",
    "path": "folders/Sounds/Effects/RPG Sound Pack/NPC/giant.yy",
  },
  "resourceVersion": "1.0",
  "name": "sound_giant4",
  "tags": [],
  "resourceType": "GMSound",
}