{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sound_swing",
  "duration": 0.0,
  "parent": {
    "name": "battle",
    "path": "folders/Sounds/Effects/RPG Sound Pack/battle.yy",
  },
  "resourceVersion": "1.0",
  "name": "sound_swing",
  "tags": [],
  "resourceType": "GMSound",
}