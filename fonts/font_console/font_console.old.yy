{
    "id": "b457de92-7943-4bea-bd87-0f7cda8505fb",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_console",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7c2ac64e-6a81-40b6-af1c-1f2e59bc8d2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "bcc02bab-bd83-45f7-9e69-189b1168bc99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 24,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "518cc935-ebed-47e5-a0fc-94edbd4fe911",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 18,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c26bae82-6198-490e-b634-5fe5db467224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 62
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "213c0558-939c-4c03-8bb1-da90fcb2e955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "dc16373c-96b6-4322-9fdf-9dc5e6824893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 47
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ecd76ee9-4304-4d6d-a594-c4842cf8344f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 105,
                "y": 47
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c7b206dd-14bc-45ab-a27c-129ba96c4181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 101,
                "y": 47
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "150d2468-25f2-4de0-80f1-f203cf8b1018",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 95,
                "y": 47
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6f0b42a0-f2b3-4b02-87f8-5e78edf2ba4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 89,
                "y": 47
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "002991a7-3710-4aed-b1f8-addbe5e2f8d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 28,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b1c13e6f-f475-4c9d-853d-d101f08fb839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 81,
                "y": 47
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9d814a25-7f27-4740-a0d9-9105405e3734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 68,
                "y": 47
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d19bbe28-559e-4b94-a1bd-ca31b55badcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 62,
                "y": 47
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9dc51064-36ef-4828-82cf-25203ca21b23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 58,
                "y": 47
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d26575c2-653a-4865-9dc4-74fafc201712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 47
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2a62d44d-e374-49af-a266-305cdaf58fd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 47
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8f013b5b-b5c3-4287-b7e7-8a4becfe38fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 47
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d986f37f-0484-4eec-a7d0-6b78284db764",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 47
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1ffa3e9d-56a2-4a07-8f46-b5d607b99548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 47
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "cfcc0175-f5eb-47dd-bc76-ab61cb7c8a69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 47
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "52bde123-01f2-4507-bff5-13c7695c8bcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 73,
                "y": 47
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "cb5c79aa-3411-415b-a528-d7ced1512592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 44,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "cc057171-0243-4010-bdfc-79660467f382",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 77
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "02a20a4e-4ca0-4143-a50a-f0e70178c066",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 52,
                "y": 62
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f88546fc-e7b4-45f3-8d60-38cbf0a89c70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 79,
                "y": 77
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cb383201-bfd8-4bb1-b324-c97fb5827a19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 75,
                "y": 77
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "8b5df0e2-0417-437f-93c2-daa9e1f0da34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 70,
                "y": 77
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9d506cdd-a53d-49bb-8b76-70e8266e1e91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 63,
                "y": 77
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3b6aa97f-f81c-4f1f-b10e-30d134c45701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 55,
                "y": 77
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "19d690ad-a433-4459-94d6-94c6b9d47fd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 48,
                "y": 77
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d003fd1c-cba3-4668-9acb-a34362ea9229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 42,
                "y": 77
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d4880ed0-d8fd-4a84-962e-c0498b3b1ba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 77
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "eaf57985-056a-4580-ad6c-ff7913599284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 77
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "30d5842d-8cd5-4533-973f-cff005159214",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 87,
                "y": 77
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "80c9d739-0fde-4b99-8e46-abafb6873b42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 77
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9064e8b5-20e0-4570-97fb-2ff6c07dfc9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "62f451e3-1932-45bb-bb7e-3dd317bd685d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 113,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bda09d0a-7c56-48a5-9ea6-6f1b780b7bc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 106,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "cf0c8a75-23ac-498e-b7ac-7a74756fe1e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "43643523-bba4-4ad5-af07-e058a451fda4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6b039482-84ab-41ca-83a4-64ba807fd65c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2f796ea7-9691-4cde-bf0a-076aca7e8f22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 75,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "fff05656-800a-4708-8b2f-3e8e6950d17c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 67,
                "y": 62
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "98984d7b-4efc-44c4-9092-bfa42422d77e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 60,
                "y": 62
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "628cf204-1383-4532-929a-06b0f179f14b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f78ee4ff-08f0-446c-bebb-f99fc2895cd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 36,
                "y": 62
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e120bf4b-2dba-486d-b01c-0c32e4e33729",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 119,
                "y": 32
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "db0979b1-6bb8-417d-88f1-a73d896899ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 51,
                "y": 17
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2ba1403b-d15d-4af8-94a5-c4b2f3b8008d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 34,
                "y": 17
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c5b4307e-40aa-4b43-b320-24aa845e6c89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 17
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ea769cfc-b52b-49e1-8605-63c2c99b7b10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 17
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "948f0ba8-8e58-4690-bff1-5597c4756b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 17
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4b206541-bce1-41aa-98da-0234506cb6e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "37d80175-f0ca-45c9-b271-29ecd04849cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ffdb465e-e739-4ffa-bdd4-3b0c32a89a7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "79170ed3-496a-40fb-b0ce-aa2e23620409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5d3dcd45-f939-4c35-bd20-6f58b4f00251",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5de1c73e-e5a1-4f89-ac4d-2ac602edcd2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 43,
                "y": 17
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b82bbe05-b579-49b1-badb-29f2e2c00c43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "947ecc15-39af-420e-9192-032ebc6307ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4dfcb5a7-1556-4c24-a7e4-a96d9969b56e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "cec03e95-77db-4c46-a7f3-44380b3503cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0a844f85-9660-426f-ac80-94844a1f2775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9df399a2-a031-4a0f-a050-951c625a09ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4c82382c-d711-4533-88b9-08f73d533371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b4f763d2-32ee-4380-ad70-93b144914f0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8d726890-a066-4df1-9e8a-48cab500d654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0dd66051-fed5-49c7-8128-176a8e64ac0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f8ba0df1-b2c2-4947-9d61-446571e6a84d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1412f923-0cba-48f5-ac03-bee2c154774f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 59,
                "y": 17
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e337a799-3369-4f84-a676-e021a3f55adf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 32
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a53ecb10-be49-40db-b046-0fd75ee2b4e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 67,
                "y": 17
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8237f526-818c-4854-a9dc-5983497aec2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 97,
                "y": 32
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7bdde682-9b4a-4489-a319-fb2f086eae09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 90,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1281607a-a944-4bec-9352-b26a74edf112",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 32
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d676b931-1172-4c02-9016-78ad66ad249a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 32
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ff1ef86a-085d-4683-bad2-aa1541d3a949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 32
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3e4ab683-6ed9-4889-89cd-6c37ca6044d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 32
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "92df247c-48bf-40a6-8b1c-a3028b4f4339",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3158a46a-ac63-417c-80dc-6c84c3871b06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "264f444b-efd3-4c1e-882d-dafb10db5ca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7b2410ec-9803-4026-8be3-809d60ccfd75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 105,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "09f55656-dd74-4ea5-b58d-941664431f7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ad3dd16f-63e5-4590-9926-783726a33d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "888fae4e-31d7-4491-8548-a705da41a3e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "51b53b4f-4472-464a-ad7c-3c1f980a5cb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 118,
                "y": 17
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "208e899d-a022-4414-a878-ce223b902d18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 110,
                "y": 17
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "50b0a175-f4d3-41e1-bdd0-30eec3336b9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 102,
                "y": 17
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3e0cb610-7622-4365-b02c-9729f08ec525",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 94,
                "y": 17
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4cfcaad5-e2f8-4beb-a84b-28b17d40fc6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 86,
                "y": 17
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "492bc56d-ebcd-4a11-aed6-df4c943bb6cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 17
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0b76a7e3-a669-4f41-ae9e-c3a45916c898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 75,
                "y": 17
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "21dfdabc-2f9c-4670-b328-a7e2bc08a7c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 112,
                "y": 32
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "446c3c5f-eb07-411c-a9d7-f0ff95a0f48d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 95,
                "y": 77
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}