{
    "id": "a8443db9-16e2-4a80-b1d0-58439775ed26",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_room_transistion",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3c0425f4-12ba-4892-ae37-99b196b138f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 98,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2e459fa8-a80a-4925-b55f-ed5d3a702589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 98,
                "offset": 7,
                "shift": 24,
                "w": 10,
                "x": 713,
                "y": 202
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b297f47f-cb3c-4eff-b1c7-5c86fc407c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 98,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 687,
                "y": 202
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "805a6de3-a7db-4c14-9eea-fc4fb4ac94c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 98,
                "offset": 0,
                "shift": 47,
                "w": 47,
                "x": 638,
                "y": 202
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1e73394b-0761-4692-b359-bc41dc7e16c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 98,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 595,
                "y": 202
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "88aa9dec-5c41-4d38-8b15-5660de14f6a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 98,
                "offset": 4,
                "shift": 76,
                "w": 67,
                "x": 526,
                "y": 202
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "17b151f3-5cc6-4a4a-8ea2-9d91f4bf298e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 98,
                "offset": 3,
                "shift": 57,
                "w": 52,
                "x": 472,
                "y": 202
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "af58fae7-5d19-465e-80a0-68a2d549c1e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 98,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 460,
                "y": 202
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "229b94b9-346d-4fbd-9f49-311861a6c7d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 98,
                "offset": 5,
                "shift": 28,
                "w": 21,
                "x": 437,
                "y": 202
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "112f16d0-9fba-488b-80fa-ddfe0da9ba00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 98,
                "offset": 5,
                "shift": 28,
                "w": 21,
                "x": 414,
                "y": 202
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1bcfb212-5ebf-4407-ac6f-79a1b0f51f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 98,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 725,
                "y": 202
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "926de290-6a84-4cde-b955-b260783f4249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 98,
                "offset": 4,
                "shift": 50,
                "w": 41,
                "x": 371,
                "y": 202
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "407d946d-0eb1-496c-94c1-178e4e722e24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 98,
                "offset": 7,
                "shift": 24,
                "w": 10,
                "x": 316,
                "y": 202
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1cf3eb13-447a-4956-a93d-2323321cb0b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 98,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 290,
                "y": 202
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "cf38566a-f6cd-4c66-b294-76153a298246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 98,
                "offset": 7,
                "shift": 24,
                "w": 10,
                "x": 278,
                "y": 202
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c8fca72e-8079-4557-b120-02a9d5b200b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 98,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 252,
                "y": 202
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3e68c0e6-b5ac-4dd0-bc31-dc56fe9150e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 98,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 209,
                "y": 202
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "36127485-f847-44d1-9040-c3c6ca1c318e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 98,
                "offset": 9,
                "shift": 47,
                "w": 23,
                "x": 184,
                "y": 202
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2a1f41ed-5432-4e13-8345-229a1c8f963d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 98,
                "offset": 2,
                "shift": 47,
                "w": 41,
                "x": 141,
                "y": 202
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a74865a7-d3ba-449c-a2d1-a196c63f35c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 98,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 98,
                "y": 202
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ce625499-0ee2-4990-ba86-5b69c3f3d435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 98,
                "offset": 1,
                "shift": 47,
                "w": 43,
                "x": 53,
                "y": 202
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "98678c17-35e3-4601-a37f-d70539846d38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 98,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 328,
                "y": 202
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d1ed49d1-9bde-4b6f-8419-7d409db961e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 98,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 756,
                "y": 202
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "db497af6-d7a3-4249-978c-7448e6ee945a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 98,
                "offset": 4,
                "shift": 47,
                "w": 40,
                "x": 799,
                "y": 202
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9f70aedf-5d64-4047-a54a-38f0e0a3bfc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 98,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 841,
                "y": 202
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d9d9286d-7adb-442e-b3ce-a42ee873ff75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 98,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 802,
                "y": 302
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cf784487-cb2f-4126-94e2-29d2d7f18181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 98,
                "offset": 7,
                "shift": 24,
                "w": 10,
                "x": 790,
                "y": 302
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "23d390bf-1a67-4b1d-9bd7-5978a1a27837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 98,
                "offset": 7,
                "shift": 24,
                "w": 10,
                "x": 778,
                "y": 302
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f08e8e5c-b675-4e58-a5c8-ae3e545bae52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 98,
                "offset": 4,
                "shift": 50,
                "w": 41,
                "x": 735,
                "y": 302
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "13b77fe1-1b95-4265-a383-d79509c30b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 98,
                "offset": 4,
                "shift": 50,
                "w": 41,
                "x": 692,
                "y": 302
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "75e55fed-ca25-4fd5-ac32-660a930fc397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 98,
                "offset": 4,
                "shift": 50,
                "w": 41,
                "x": 649,
                "y": 302
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "89692883-3b04-4b7e-bfa2-9ddb7f53b7a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 98,
                "offset": 3,
                "shift": 47,
                "w": 40,
                "x": 607,
                "y": 302
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2447f05e-14f8-4b81-935d-5c9039c5409b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 98,
                "offset": 4,
                "shift": 86,
                "w": 80,
                "x": 525,
                "y": 302
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c572e66d-adbb-41f5-bc17-2dfa084e60a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 98,
                "offset": -1,
                "shift": 57,
                "w": 58,
                "x": 465,
                "y": 302
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b24940c3-181f-4874-beca-fddd215f0e42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 98,
                "offset": 6,
                "shift": 57,
                "w": 47,
                "x": 416,
                "y": 302
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0514cfef-6a20-4db5-8737-f785e169bbaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 98,
                "offset": 4,
                "shift": 61,
                "w": 55,
                "x": 359,
                "y": 302
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c6355638-a054-4c20-a3c4-a847671ad370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 98,
                "offset": 6,
                "shift": 61,
                "w": 51,
                "x": 306,
                "y": 302
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a305055c-9825-4b02-9a1d-a80fad0bbf90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 98,
                "offset": 6,
                "shift": 57,
                "w": 47,
                "x": 257,
                "y": 302
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6e84222d-0563-48ee-8474-66181ac71143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 98,
                "offset": 6,
                "shift": 52,
                "w": 43,
                "x": 212,
                "y": 302
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8b13da92-dd90-4590-95ba-45bb650e78aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 98,
                "offset": 4,
                "shift": 66,
                "w": 57,
                "x": 153,
                "y": 302
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d0daff55-e5c3-494f-abcc-0952c6c73142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 98,
                "offset": 6,
                "shift": 61,
                "w": 49,
                "x": 102,
                "y": 302
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "589e16aa-d8d0-4735-a006-49c6b5aa99c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 98,
                "offset": 7,
                "shift": 24,
                "w": 9,
                "x": 91,
                "y": 302
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "66842641-569c-4436-8503-1b87a1a0dd12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 98,
                "offset": 2,
                "shift": 43,
                "w": 34,
                "x": 55,
                "y": 302
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7e20f88c-de02-4606-b04d-f38448bc4fcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 98,
                "offset": 6,
                "shift": 57,
                "w": 51,
                "x": 2,
                "y": 302
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "03c0bd6c-e2e0-4939-ad5e-85aa27f2be2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 98,
                "offset": 6,
                "shift": 47,
                "w": 39,
                "x": 945,
                "y": 202
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7d85d0b6-5e56-4bee-b607-dc8ca50e487b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 98,
                "offset": 6,
                "shift": 71,
                "w": 59,
                "x": 884,
                "y": 202
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "322c6dd6-29e0-458e-8de8-2d8210b467a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 98,
                "offset": 6,
                "shift": 61,
                "w": 49,
                "x": 2,
                "y": 202
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f59373a5-9211-4438-b4e0-c719160b3c77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 98,
                "offset": 4,
                "shift": 66,
                "w": 59,
                "x": 934,
                "y": 102
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1032be5f-ca6b-4f64-8934-3f307c27cca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 98,
                "offset": 6,
                "shift": 57,
                "w": 47,
                "x": 885,
                "y": 102
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "dcb9cdf0-875c-4119-909a-716018e66b71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 98,
                "offset": 3,
                "shift": 66,
                "w": 60,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2976dace-6557-41df-a4c4-9a1232b65c13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 98,
                "offset": 6,
                "shift": 61,
                "w": 55,
                "x": 886,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "85fb4895-8e72-43f6-818f-35baa64d87cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 98,
                "offset": 3,
                "shift": 57,
                "w": 50,
                "x": 834,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0fd88d9f-f725-4af7-b941-76427cc587f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 98,
                "offset": 2,
                "shift": 52,
                "w": 49,
                "x": 783,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ef73b09a-bb5b-458d-bd15-5f8596bb8b28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 98,
                "offset": 6,
                "shift": 61,
                "w": 49,
                "x": 732,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8b000568-49ac-4fa4-881a-dd8e2841322f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 98,
                "offset": 0,
                "shift": 57,
                "w": 57,
                "x": 673,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c03aa1d4-78f6-47f4-9342-c1b8603368f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 98,
                "offset": 1,
                "shift": 80,
                "w": 79,
                "x": 592,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "903217c8-bf4a-46d3-acd5-871e4572f0c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 98,
                "offset": 0,
                "shift": 57,
                "w": 57,
                "x": 533,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2c208081-e6e1-4e36-bc8c-04c75fe589c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 98,
                "offset": 0,
                "shift": 57,
                "w": 57,
                "x": 474,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "681cfb6d-3ce3-41b0-ba03-18c389768dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 98,
                "offset": 1,
                "shift": 52,
                "w": 49,
                "x": 423,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "aa6833bf-7429-420e-9d55-2d4cd63fcc61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 98,
                "offset": 5,
                "shift": 24,
                "w": 18,
                "x": 943,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3add43ce-165e-4f1a-98cc-a7499d6f0f4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 98,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 397,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4726b843-694c-4682-a5a4-91f6416ecb1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 98,
                "offset": 1,
                "shift": 24,
                "w": 18,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "3a08baa2-bda9-45d5-94c9-722ede41a117",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 98,
                "offset": 2,
                "shift": 40,
                "w": 36,
                "x": 310,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3f6b430b-3edd-4a28-a552-1bd8fa290769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 98,
                "offset": -2,
                "shift": 47,
                "w": 51,
                "x": 257,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8de8d320-cf71-4054-a963-71142f681db7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 98,
                "offset": 3,
                "shift": 28,
                "w": 17,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "afbbbabe-314a-4ab6-8af4-0b0b4e55c0ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 98,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4994f8c3-7ad0-4647-8895-a2afd1a97da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 98,
                "offset": 5,
                "shift": 47,
                "w": 39,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1c6be28f-19a1-4e72-a881-bd7761faed15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 98,
                "offset": 3,
                "shift": 43,
                "w": 39,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7efa6fb7-13b1-4e9a-a6e3-5caae7d22864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 98,
                "offset": 2,
                "shift": 47,
                "w": 40,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ca2e8fb1-8752-4ced-9f08-583cc1d4eb9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 98,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0643060f-e82e-4132-9ce0-fa2ff76fbe26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 98,
                "offset": 0,
                "shift": 24,
                "w": 27,
                "x": 368,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a74386cf-5d75-45d6-9760-f7ae451d629a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 98,
                "offset": 2,
                "shift": 47,
                "w": 40,
                "x": 64,
                "y": 102
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5dc49f5f-1774-4c5c-9e89-32644f32a1ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 98,
                "offset": 5,
                "shift": 47,
                "w": 37,
                "x": 455,
                "y": 102
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "93de05bc-8f4f-4e7c-88fd-a27610886cba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 98,
                "offset": 5,
                "shift": 19,
                "w": 9,
                "x": 106,
                "y": 102
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c804e383-aa23-4c5b-aae7-fc5776cb5526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 98,
                "offset": -4,
                "shift": 19,
                "w": 18,
                "x": 825,
                "y": 102
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6f3500b8-96bf-46a6-9ce8-46fd80b0c03d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 98,
                "offset": 5,
                "shift": 43,
                "w": 38,
                "x": 785,
                "y": 102
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b287318f-ace3-4728-b856-9ee50cc1b824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 98,
                "offset": 5,
                "shift": 19,
                "w": 8,
                "x": 775,
                "y": 102
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "202736fb-5506-41e2-8a09-9ba2c3e567e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 98,
                "offset": 5,
                "shift": 71,
                "w": 61,
                "x": 712,
                "y": 102
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "984daf36-79ce-4633-87a0-11c09c5a88e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 98,
                "offset": 5,
                "shift": 47,
                "w": 37,
                "x": 673,
                "y": 102
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "34f0000e-ebb2-47f4-abb8-918491677037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 98,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 628,
                "y": 102
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "bad3aabc-e19d-4b5f-9ec4-c33f195f2990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 98,
                "offset": 5,
                "shift": 47,
                "w": 39,
                "x": 587,
                "y": 102
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2d8ff43f-2fb5-43be-b93f-1c74d3035080",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 98,
                "offset": 2,
                "shift": 47,
                "w": 40,
                "x": 545,
                "y": 102
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8572e84b-735c-4655-b2d1-013b3b92ff7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 98,
                "offset": 5,
                "shift": 28,
                "w": 25,
                "x": 518,
                "y": 102
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0b51b8bf-feab-4685-b9fc-9f7fa865ced0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 98,
                "offset": 2,
                "shift": 43,
                "w": 38,
                "x": 845,
                "y": 102
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "04663a93-64ae-4e4a-a551-cbd8593332b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 98,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 494,
                "y": 102
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "82fa55b4-e407-4ad6-92aa-9f52ca205cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 98,
                "offset": 5,
                "shift": 47,
                "w": 37,
                "x": 416,
                "y": 102
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c6564e25-1b10-4fde-9049-5b09c3dbdfd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 98,
                "offset": 1,
                "shift": 43,
                "w": 41,
                "x": 373,
                "y": 102
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ce55e044-4751-4318-9cd1-d2c3b46768c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 98,
                "offset": 0,
                "shift": 61,
                "w": 61,
                "x": 310,
                "y": 102
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "cffe2135-2537-425a-a3e9-f4151c578c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 98,
                "offset": 0,
                "shift": 43,
                "w": 42,
                "x": 266,
                "y": 102
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fcac8123-f15e-4ea0-a345-ab5dc119f086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 98,
                "offset": 1,
                "shift": 43,
                "w": 41,
                "x": 223,
                "y": 102
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "db75aae3-5ddf-41d3-80eb-77a431abbd00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 98,
                "offset": 1,
                "shift": 43,
                "w": 40,
                "x": 181,
                "y": 102
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "46ea86de-b342-46b0-89cd-203fb9a5575b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 98,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 154,
                "y": 102
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e96b1bcd-b083-4f1c-b238-768dd8f9cee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 98,
                "offset": 7,
                "shift": 22,
                "w": 8,
                "x": 144,
                "y": 102
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a3fc1416-3958-4c40-8aa6-8109bbbd296c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 98,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 117,
                "y": 102
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "693787a4-2b67-49ad-b9d4-0ccedb27f8ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 98,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 845,
                "y": 302
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "ddf441e7-e4be-4d72-bfe1-904de93d691c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 98,
                "offset": 17,
                "shift": 82,
                "w": 49,
                "x": 891,
                "y": 302
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "2e1a21fa-b838-444c-ad5a-0eefa192022a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 32,
            "second": 65
        },
        {
            "id": "1fa64d4d-93c6-43f9-ae9a-49af1c7478cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 84
        },
        {
            "id": "5716f3c0-40f9-433a-bdfc-4195bd1e491a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 89
        },
        {
            "id": "93d2917a-7aca-475c-b295-090d21ecfdc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 32,
            "second": 902
        },
        {
            "id": "c5c89bc7-ab80-42a8-be51-d7f3033e3356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 32,
            "second": 913
        },
        {
            "id": "5deb08fd-b0ef-44fd-865e-fb5af9096db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 32,
            "second": 916
        },
        {
            "id": "e57f8e13-0220-439f-b121-61969ae15aa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 32,
            "second": 923
        },
        {
            "id": "92d1abce-d957-44ff-ab35-10a70a61115e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 932
        },
        {
            "id": "144e0774-a7bc-4594-87fd-cb0b70883ce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 933
        },
        {
            "id": "5b468576-1cd6-41fa-837c-5af5fe6b75b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 939
        },
        {
            "id": "ef0c9e6f-5633-46ba-92e5-d248bacca48c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 49
        },
        {
            "id": "d38fc78b-b5c2-41e6-b633-de356b0d327d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 32
        },
        {
            "id": "9f18e962-f9b9-4e3c-be1c-23dfa80e904c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 84
        },
        {
            "id": "4b9ed600-2a81-4869-90e5-a3cf639f7b4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 86
        },
        {
            "id": "1a8266eb-e3e8-47eb-b605-2ab313e328b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 87
        },
        {
            "id": "be42daef-d16c-470a-84aa-81bbc87e36cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 89
        },
        {
            "id": "1938a7d9-c553-4f06-a6d0-92d228a27d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "e92f7cce-1180-4ef4-a416-1d8db94fbdeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 119
        },
        {
            "id": "15700545-7594-42f9-8c07-ca73609ac68f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 121
        },
        {
            "id": "d6e7aa18-2031-4d1d-a00b-d3376f6aabc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 160
        },
        {
            "id": "d23c880c-7144-4614-8602-94d4bbf0f0da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 8217
        },
        {
            "id": "c277ef06-69b7-4c4f-a99e-301ff8a314c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 44
        },
        {
            "id": "9bd4fa28-7dde-45ef-9ed4-405dd4ff9cb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 46
        },
        {
            "id": "bfc20748-5174-451a-a367-78709a16cd47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 65
        },
        {
            "id": "42ae4ba5-2da3-49a6-bc92-2ab8f52bc740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 32
        },
        {
            "id": "134ee9b8-af47-45e3-9e0f-3782598d767e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 84
        },
        {
            "id": "8e998a22-0dc1-4ee4-ac19-ca54eec40434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 86
        },
        {
            "id": "9ba6cde4-bbcb-4af3-a25d-aa162d707a84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 87
        },
        {
            "id": "4d467ea7-a0b8-4a18-be2a-33d8e408595c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 89
        },
        {
            "id": "88cd0e2b-3017-4ac8-91f8-e8c666ecc704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 121
        },
        {
            "id": "dd020c4d-3f82-4a26-ba8d-b60920a7d367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 160
        },
        {
            "id": "396eb532-f6e5-448d-a2ef-3788d354d4f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 8217
        },
        {
            "id": "684a71d5-c68f-4b80-ba25-7151b9d4ec18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 32
        },
        {
            "id": "25c7ccd3-996b-4e25-8280-687fcd7a0032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 80,
            "second": 44
        },
        {
            "id": "b045c503-465f-4b4f-ab9e-b8040e3b431b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 80,
            "second": 46
        },
        {
            "id": "a668126b-2677-4129-89f1-f05b2a4ca7b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 65
        },
        {
            "id": "0f98e412-e58a-4a07-9864-0deefcde52d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 160
        },
        {
            "id": "a1cfb5ca-e78d-4730-a46e-7aec5d711080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "3f583bca-31d1-44b4-8a01-9b35770a84a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 86
        },
        {
            "id": "632b7816-284b-4d60-922b-7c62fa114b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 87
        },
        {
            "id": "23c6cc61-df54-4405-ad0d-b3fb518e6e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "4296ce89-fcfa-4227-98f9-7a5a2d4c0ede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 32
        },
        {
            "id": "109f15f6-bd5f-48b4-b5a3-ba5ee3020da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 44
        },
        {
            "id": "a0e36511-bbbf-4a27-823c-66fed528da0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 45
        },
        {
            "id": "7283fe86-fcde-4acb-9c97-95385e41d26e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 46
        },
        {
            "id": "799a7d9a-0a6f-45fd-8cf2-cba392624c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 58
        },
        {
            "id": "32efa8ca-8670-44cf-8c0f-e0f30c6e12ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 59
        },
        {
            "id": "503d2f1b-5da3-4d31-936e-3386f88a6526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 65
        },
        {
            "id": "184f13d0-2b1b-4ebf-82e0-6d1be7811fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 79
        },
        {
            "id": "8271bdd0-53a3-44e9-bcc0-847f2fef99a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 97
        },
        {
            "id": "acd0d3be-5490-49b3-9921-d88f9971d86f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 99
        },
        {
            "id": "7f7f2dd0-4e38-4d9b-9f42-690020c902ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 101
        },
        {
            "id": "1e733cbc-3020-456c-9bc7-30955a589b1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 105
        },
        {
            "id": "81b52188-b10f-4220-91a0-f88a52e48e53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 111
        },
        {
            "id": "48db2d85-744f-4194-be42-83581ba1f160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 114
        },
        {
            "id": "87837cbe-cae6-4b59-b9aa-dbd4612dcd06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 115
        },
        {
            "id": "70204565-beca-4f7c-b5ea-34ae396db37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 117
        },
        {
            "id": "3377ada1-ce82-4ef4-934c-cb1eafdd6a49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 119
        },
        {
            "id": "293e914d-6890-4841-a45c-0c5d6d69f647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 121
        },
        {
            "id": "aaf2535c-f18e-41d2-bcea-90694787e40c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 160
        },
        {
            "id": "ca487c1c-4f44-4219-8965-a1d4daa6cbcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 173
        },
        {
            "id": "6e4b4433-6c4b-47b9-80b5-d905e3b19dd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 894
        },
        {
            "id": "6c1deb6c-c40e-43fd-a3e8-88ab89e66ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 44
        },
        {
            "id": "d84c6790-86d7-4023-a937-e3107685bd34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 45
        },
        {
            "id": "bd20e09e-bee4-4d01-add1-01ebcc592b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 46
        },
        {
            "id": "9d29f12e-b9ed-43fd-a51c-119a4b6b45f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 58
        },
        {
            "id": "a7abad0d-eae9-488f-bd68-305d5b12f1ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 59
        },
        {
            "id": "58606313-7c32-4ca1-9f89-3adf305f34fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 65
        },
        {
            "id": "ce386cf7-784a-42d5-b688-3a649075a975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 97
        },
        {
            "id": "df66eb5e-ec6e-40d7-8e44-02f20977c02c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 101
        },
        {
            "id": "5cced1c5-c4b9-47e3-b033-4e14109cf25b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 105
        },
        {
            "id": "405577a0-ccd3-4c70-8078-7ebbad130980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 111
        },
        {
            "id": "6db5aeaa-fdfd-4593-926d-93c00e3054a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 114
        },
        {
            "id": "5d9ce8e4-54fe-41f6-9d6e-6c0479c74924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 117
        },
        {
            "id": "0ac813b8-4964-4cbf-869b-936f8758f357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 121
        },
        {
            "id": "d4eb9779-f9f6-478e-bcdc-e3dc38d41da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 173
        },
        {
            "id": "78300226-9571-4a13-884f-953ea6197851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 894
        },
        {
            "id": "342b2dd1-43b5-4997-86d6-7d30dcdd9516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 44
        },
        {
            "id": "7c3a1705-0b3a-40fa-a891-17edd8a3acfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 45
        },
        {
            "id": "de8de0ab-5394-448a-b935-0b5ba9a0af27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 46
        },
        {
            "id": "d0fa0ada-055e-4bfa-b004-0434f2b2b608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 58
        },
        {
            "id": "3c80c1d5-05e9-4a8a-b811-b898cdac7fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 59
        },
        {
            "id": "75ce38df-cb96-430d-a35b-8fce1e507dfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 65
        },
        {
            "id": "d722564c-d9cd-4690-a0ce-741665b68a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 97
        },
        {
            "id": "ef72f17b-4725-4eb1-b55b-5d056704e45a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 101
        },
        {
            "id": "a5937f22-cf77-46a4-9998-447dc80c599e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 111
        },
        {
            "id": "980e14a7-477f-4b23-bfbe-41484fe5beb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 114
        },
        {
            "id": "38fc0ac5-d080-4aa5-8f9d-df53818c109a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 117
        },
        {
            "id": "e844bce7-0f45-498f-bec9-c2870250f1e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "8c25660a-3570-4d91-9ee2-de14c5766b77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 173
        },
        {
            "id": "a9c03dd0-2a53-4bda-8aab-41a4fd184da7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 894
        },
        {
            "id": "1d4b20bc-2c2f-458f-9b4e-9d1574a0cfb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 32
        },
        {
            "id": "830bb0b5-a450-443b-8d6b-17fe5209c418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 44
        },
        {
            "id": "950dde93-7708-46eb-83df-eebfb241a871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 45
        },
        {
            "id": "a473d4d3-f2e3-4d21-906c-b2edd465d33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 46
        },
        {
            "id": "2e05a1bf-98bf-46e7-92fb-228aa3e97484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 58
        },
        {
            "id": "f39f7905-aeb9-470a-bb3d-6ecd999c8971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 59
        },
        {
            "id": "f1ab2145-b0f3-4349-863b-4246b67b8a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 65
        },
        {
            "id": "c46aff28-bee0-4e93-9be9-fe4b2e4a30a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 97
        },
        {
            "id": "e2aeacf3-8bcd-4a4a-813b-d04416a0c02c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 101
        },
        {
            "id": "1a631bf7-4a93-43e2-8164-8bafbefcca30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 105
        },
        {
            "id": "8e2d1671-b86c-495a-8c0d-77f1c37849e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 111
        },
        {
            "id": "fe2e514c-8095-492a-a4bd-1408acba5c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 112
        },
        {
            "id": "657e5330-18a3-4843-93ad-0bca0d644e57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 113
        },
        {
            "id": "acb97a2e-4e7f-4edf-9ef1-c8d598d451cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 117
        },
        {
            "id": "40d0b982-576d-4316-8322-f23d61e2350a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 118
        },
        {
            "id": "0f8bdf35-1d92-43c0-9b79-67e666e69945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 160
        },
        {
            "id": "274fe771-c61f-4822-96ec-2692ac513fa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 173
        },
        {
            "id": "7eb35de1-fed8-4ed4-84d4-882dd7d73644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 894
        },
        {
            "id": "89e15428-d130-449f-a5af-d3f6492a3179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 102
        },
        {
            "id": "a456625a-8505-488e-8237-abc46835e372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8217
        },
        {
            "id": "77d39566-c6d2-4f25-96c3-6ee3b5f38692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 44
        },
        {
            "id": "bc2871a5-e172-4d2d-b4b1-e32f68b36f06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 46
        },
        {
            "id": "5e04f3a7-c32c-4cec-9682-3e2c0dd8dc76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 8217
        },
        {
            "id": "532ed766-1a19-4995-bb75-eb6841ab8adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 118,
            "second": 44
        },
        {
            "id": "589c8a98-8faf-42b1-84ec-621a491fdf89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 118,
            "second": 46
        },
        {
            "id": "c525f337-a080-4200-b540-770bd49420b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 119,
            "second": 44
        },
        {
            "id": "b5b44ca9-4d62-4b1a-a836-7ae4ca2c888d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 119,
            "second": 46
        },
        {
            "id": "fdd1429f-5aca-4773-997d-97449ac1f944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 121,
            "second": 44
        },
        {
            "id": "889e8532-f852-4514-8127-0b7a20b49579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 64,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}